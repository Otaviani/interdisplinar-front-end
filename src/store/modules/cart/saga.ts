import { all, call, put, select, takeLatest } from "redux-saga/effects";
import api from "../../../services/api";
import { addProductToCartSuccess, getBooksCartSuccess, removeBooksCartSuccess, updateQuantityBuySuccess, getBooksCart as getAndSetBooksCart } from "./actions";
import { Book, CartItem } from "./types";

function* addBookToCart(data: any) {
    try {
        const cartItems = yield select(state => state.cart.cartItems);

        if (cartItems.filter((item: CartItem) => item.uuid === data.payload.cartItem.uuid).length === 0) {
            cartItems.push(data.payload.cartItem);
        } else {
            cartItems.map((item: CartItem) => {
                item.quantity = item.uuid === data.payload.cartItem.uuid 
                    ? item.quantity + data.payload.cartItem.quantity
                    : item.quantity;
            });
        }

        yield put(addProductToCartSuccess(cartItems));

        localStorage.setItem("@library:cart", JSON.stringify(cartItems));
    } catch(err) {

    }
}

function* removeBooksCart(data: any) {
    try {
        const cartItems = yield select(state => state.cart.cartItems);

        const leftItems = cartItems.filter((item: CartItem) => {
            return item.uuid != data.payload.cartItem.uuid
        });

        yield put(removeBooksCartSuccess(leftItems));
        yield put(getAndSetBooksCart());

        localStorage.setItem("@library:cart", JSON.stringify(leftItems));
    } catch(err) {

    }
}

function* updateBooksCart(data: any) {
    try {
        const cartItems = yield select(state => state.cart.cartItems);

        console.log(cartItems);

        cartItems.map((item: CartItem) => {
            if (item.uuid === data.payload.cartItem.uuid) {
                item.quantity = data.payload.cartItem.quantity
            }
        });

        console.log(cartItems);

        yield put(updateQuantityBuySuccess(cartItems));

        localStorage.setItem("@library:cart", JSON.stringify(cartItems));
    } catch(err) {

    }
}

function* getBooksCart() {
    try {
        const cartItems = yield select(state => state.cart.cartItems);

        const response = yield call(api.post, '/reader/cart-items', cartItems.map((item: CartItem) => {
            return item.uuid
        }));

        const mergedItems = response.data.map((book: Book) => {
            var quantity = cartItems.filter((cartItem: any) => {
                return cartItem.uuid === book.uuid;
            });

            return {
                quantity: quantity[0].quantity,
                book: book 
            }
        });

        yield put(getBooksCartSuccess(mergedItems));
    } catch(err) {

    }
}

export default all([
    takeLatest('ADD_PRODUCT_TO_CART', addBookToCart),
    takeLatest('GET_CART_ITEMS', getBooksCart),
    takeLatest('REMOVE_CART_ITEM', removeBooksCart),
    takeLatest('UPDATE_CART_ITEM', updateBooksCart),
]);
