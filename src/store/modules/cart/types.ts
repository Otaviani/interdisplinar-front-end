export interface CartItem {
    uuid: string,
    quantity: number;
}

export interface Book {
    uuid: string
    name: string,
    synopsis: string,
    genre: string,
    price: number,
    quantity: string,
}

export interface Cart {
    uuid: string,
    book: Book,
}

export interface CartState {
    cartItems: CartItem[]
    booksCart: Book[],
    cart: Cart[],
}
