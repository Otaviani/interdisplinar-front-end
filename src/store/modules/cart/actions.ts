import { Book, Cart, CartItem } from "./types";

export function addProductToCart(cartItem: CartItem) {
    return {
        type: 'ADD_PRODUCT_TO_CART',
        payload: {
            cartItem,
        }
    }
}

export function addProductToCartSuccess(cartItems: Array<CartItem>) {
    return {
        type: 'ADD_PRODUCT_TO_CART_SUCCESS',
        payload: {
            cartItems,
        }
    }
}

export function getBooksCart() {
    return {
        type: 'GET_CART_ITEMS',
    }
}

export function getBooksCartSuccess(booksCart: CartItem) {
    return {
        type: 'GET_CART_ITEMS_SUCCESS',
        payload: {
            booksCart
        }
    }
}

export function removeBooksCart(cartItem: CartItem) {
    return {
        type: 'REMOVE_CART_ITEM',
        payload: {
            cartItem,
        }
    }
}

export function removeBooksCartSuccess(cartItems: Array<CartItem>) {
    return {
        type: 'REMOVE_CART_ITEM_SUCCESS',
        payload: {
            cartItems,
        }
    }
}

export function updateQuantityBuy(cartItem: CartItem) {
    return {
        type: 'UPDATE_CART_ITEM',
        payload: {
            cartItem,
        }
    }
}

export function updateQuantityBuySuccess(cartItems: Array<CartItem>) {
    return {
        type: 'UPDATE_CART_ITEM_SUCCESS',
        payload: {
            cartItems,
        }
    }
}
