import { CartState } from "./types";

const INITIAL_STATE: CartState = {
    cartItems: JSON.parse(localStorage.getItem("@library:cart") ?? '') ?? [],
    booksCart: [],
    cart: [],
}

export default function cart(state = INITIAL_STATE, action: any) {
    switch (action.type) {
        case 'ADD_PRODUCT_TO_CART': 
            return {
                ...state,
            }
        case 'ADD_PRODUCT_TO_CART_SUCCESS': 
            return {
                ...state,
                cartItems: action.payload.cartItems
            }
        case 'GET_CART_ITEMS':
            return {
                ...state,
            }
        case 'GET_CART_ITEMS_SUCCESS':
            return {
                ...state,
                booksCart: action.payload.booksCart
            }
        case 'REMOVE_CART_ITEM':
                return {
                    ...state,
                }
        case 'REMOVE_CART_ITEM_SUCCESS':
            return {
                ...state,
                cartItems: action.payload.cartItems
            }
        case 'UPDATE_CART_ITEM':
                return {
                    ...state,
                }
        case 'UPDATE_CART_ITEM_SUCCESS':
            return {
                ...state,
                cartItems: action.payload.cartItems
            }
        default:
            return state
    }
}
