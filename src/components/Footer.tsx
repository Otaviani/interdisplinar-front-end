import React from 'react';

import '../styles/components/footer.css';

export default function Footer(props: any) {
    return (
        <footer id="footer" className="d-flex justify-content-between align-items-center p-3">
            { props.children }
        </footer>
    )
}