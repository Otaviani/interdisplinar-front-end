import React, { useEffect, useState } from 'react';

import '../styles/components/input.css';

interface Input {
    label: string,
    type?: string,
    placeholder?: string,
    value?: string|number,
    onChange?: Function,
    onBlur?: Function,
    disabled?: boolean,
}

export default function Input(props: Input) {
    const [value, setValue] = useState<any>('');

    useEffect(() => {
        if (props.onChange) {
            props.onChange(value);
        }
    }, [value])

    function onBlur() {
        if (props.onBlur) {
            props.onBlur(value);
        }
    }

    return (
        <label className="label">
            { props.label }:
            <input 
                type={props.type ?? 'text'} 
                className="input" 
                placeholder={props.placeholder ?? ''} 
                value={props.value ?? ''}
                onChange={event => setValue(event.target.value)}
                onBlur={() => onBlur()}
                disabled={props.disabled ?? false} />
        </label>
    )
}
