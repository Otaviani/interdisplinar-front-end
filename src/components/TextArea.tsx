import React, { useEffect, useState } from 'react';

import '../styles/components/textarea.css';

interface Input {
    label: string,
    rows?: number,
    placeholder?: string,
    value?: string,
    onChange?: Function,
}

export default function TextArea(props: Input) {
    const [value, setValue] = useState<any>('');

    useEffect(() => {
        if (props.onChange) {
            props.onChange(value);
        }
    }, [value])

    return (
        <label className="label">
            { props.label }
            <textarea 
                rows={props.rows ?? 5} 
                placeholder={props.placeholder ?? ''} 
                className="textarea" 
                value={props.value ?? ''} 
                onChange={event => setValue(event.target.value)}
                style={{resize: 'none'}} 
            />
        </label>
    )
}