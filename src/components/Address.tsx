import React, { useState } from 'react';
import { BsX } from 'react-icons/bs';
import api from '../services/api';

import "../styles/components/address.css";
import Input from './Input';

interface IAddress {
    uuid: string,
    state: string,
    city: string,
    cep: string,
    neighborhood: string,
    street: string,
    number: string,
    complement: string,
}

export default function Address(props: IAddress) {
    function deleteAddress() {
        api.delete(`/reader/${props.uuid}/delete`)
    }

    return (
        <div>
            <div className="col-12 address d-flex" data-toggle="modal" data-target="#updateAddress" onClick={event => {
                    event.preventDefault();
                    event.stopPropagation();
                }}>
                <div className="col-11 d-flex align-items-center">
                    <p className="col-4" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{props.street + ", " + props.number}</p>
                    <p className="col-3" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{props.city}</p>
                    <p className="col-2" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{props.neighborhood}</p>
                    <p className="col-1" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{props.state}</p>
                    <p className="col-2 text-right" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{props.cep}</p>
                </div>

                <div className="col-1 d-flex align-items-center justify-content-center" onClick={event => {
                    event.preventDefault();
                    event.stopPropagation();
                    deleteAddress();
                }}>
                    <span className="exclude">
                        <BsX size={24} style={{color: '#FF9292'}}/>
                    </span>
                </div>
            </div>
        </div>
    );
}
