import React, { useState } from 'react';

import '../../styles/components/card/book-card.css';
import '../../styles/components/card/unavaliable-card.css';
import '../../styles/components/card/user-card.css';
import '../../styles/components/card/admin-card.css';

import { useHistory } from 'react-router-dom';
import { BsPencil } from 'react-icons/bs';
import api from '../../services/api';

interface Book {
    "uuid": string,
    "isbn": string,
    "name": string,
    "synopsis": string,
    "genre": string,
    "cover": string,
    "quantity": number,
    "price": number,
}

export default function BookAdminCard(book: Book) {
    const history = useHistory();

    const [quantity, setQuantity] = useState(book.quantity);
    const [price, setPrice] = useState(book.price);

    const [previewPrice, setPreviewPrice] = useState(book.price);
    const [previewQuantity, setPreviewQuantity] = useState(book.quantity);

    function redirectToBook(uuid: string) {
        history.push(`/configuracoes/livros/${uuid}`);
    }

    async function updateBookPrice() {
        if (previewPrice !== price) {
            await api.patch(`/settings/books/${book.uuid}/update-price`, {
                price: parseFloat(previewPrice.toFixed(2))
            }).then(response => {
                if (response.status === 200) {
                    setPrice(previewPrice);
                }
            });
        }
    }

    async function updateBookQuantity() {
        if (previewQuantity !== quantity) {
            await api.patch(`/settings/books/${book.uuid}/update-quantity`, {
                quantity: previewQuantity
            }).then(response => {
                if (response.status === 200) {
                    setQuantity(previewQuantity);
                }
            });
        }
    }
    
    return (
        <div className="col-12 card-book d-flex align-items-center" onClick={() => {redirectToBook(book.uuid)}}>
            <img src={book.cover} alt="book"/>
            <div className="card-info">
                <div className="mb-2">
                    <h3 style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{book.name}</h3>

                    <div>
                        <p style={{whiteSpace: "nowrap"}} className="d-flex">ISBN: {book.isbn}</p>
                        <p>{book.genre}</p>
                    </div>
                </div>
                
                <p>{book.synopsis}</p>
            </div>

            <div className="card-data">
                <div className="admin-card">
                    <div className="admin-info">
                        <form onClick={(event) => {
                                        event.stopPropagation();
                                    }}>
                            <label>
                                Quantidade:
                                <br/>
                                <input 
                                    name="quantity"
                                    type="number"
                                    value={previewQuantity}
                                    onBlur={updateBookQuantity}
                                    onChange={event => setPreviewQuantity(
                                        parseInt(event.target.value)
                                        ? parseInt(event.target.value)
                                        : 0
                                    )}
                                />
                            </label>
                                
                            <label>
                                Valor Unitário:
                                <br/>
                                <span>R$: </span>
                                    <input 
                                        name="price"
                                        type="number"
                                        value={previewPrice}
                                        onBlur={updateBookPrice}
                                        onChange={event => setPreviewPrice(
                                            parseFloat(event.target.value)
                                            ? parseFloat(event.target.value)
                                            : 0
                                        )}
                                        onClick={(event) => {
                                            event.stopPropagation();
                                        }}
                                    />
                            </label>
                        </form>
                    </div>
                                    
                    <span className="update">
                        <BsPencil size={20}/>
                    </span>
                </div>
            </div>
        </div>
    );
}
