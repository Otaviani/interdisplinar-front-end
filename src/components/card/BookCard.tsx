import React, { useEffect, useState } from 'react';

import '../../styles/components/card/book-card.css';
import '../../styles/components/card/unavaliable-card.css';
import '../../styles/components/card/user-card.css';
import '../../styles/components/card/admin-card.css';

import { useHistory } from 'react-router-dom';
import { BsX } from 'react-icons/bs';
import ErrorMessage from '../ErrorMessage';
import { useDispatch, useSelector } from 'react-redux';
import { removeBooksCart, updateQuantityBuy } from '../../store/modules/cart/actions';

interface ItemCart {
    quantity: number,
    book: Book,
}

interface BookItem {
    uuid: string,
    quantity: number
}

interface Book {
    uuid: string,
    name: string,
    synopsis: string,
    genre: string,
    cover: string,
    quantity: number,
    price: number,
}

export default function BookCard(itemCart: ItemCart) {
    const storageItemsCart = useSelector((state: any) => state.cart.cartItems);
    const bookItem = storageItemsCart.find((bookItem: BookItem) => bookItem.uuid === itemCart.book.uuid);

    const dispatch = useDispatch();
    const history = useHistory();

    const [quantity, setQuantity] = useState(bookItem ? bookItem.quantity : 0);

    function redirectToBook() {
        history.push(`/livros/${itemCart.book.uuid}`);
    }

    function excludeBook() {
        dispatch(removeBooksCart(itemCart.book))
    }
    
    return (
        <div className="col-12 card-book d-flex align-items-center" onClick={redirectToBook}>
            <img src={itemCart.book.cover} alt="book"/>

            <div className="card-info">
                <div className="mb-2">
                    <h3 style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{itemCart.book.name}</h3>
                    <p>{itemCart.book.genre}</p>
                </div>

                <p>{itemCart.book.synopsis}</p>
            </div>

            <div className="card-data">
                {  
                    quantity <= itemCart.book.quantity  ? 
                    (
                        Available(itemCart.book, quantity, excludeBook, setQuantity)
                    ) : (
                        Unvaliable(excludeBook)
                    )
                }
            </div>
        </div>
    );
}

function Available(book: Book, quantity: number, excludeBook: Function, setQuantity: Function) {
    const dispatch = useDispatch();
    const [amount, setAmount] = useState(quantity);

    function updateStorageQuantity() {
        dispatch(updateQuantityBuy({
            uuid: book.uuid,
            quantity: amount
        }));

        setQuantity(amount);
    }

    return (
        <div className="user-card">
            <div className="user-info">
                <form onClick={event => {
                    event.stopPropagation();
                }}>
                    <label>
                        Unidades
                        <br/>
                        <div>
                            <input 
                                name="amount" 
                                type="number" 
                                value={amount}
                                min={1}
                                max={book.quantity}
                                onChange={event => setAmount(
                                    isNaN(parseInt(event.target.value)) 
                                        ? 1 
                                        : parseInt(event.target.value)
                                )}
                                onBlur={updateStorageQuantity}
                            />
                        </div>
                    </label>

                    <label>
                        Total
                        <br/>
                        <div>R$: {(amount * book.price).toFixed(2)}</div>
                    </label>
                </form>
            </div>
        
            <span className="exclude" onClick={event => {
                event.stopPropagation();
                excludeBook();
            }}>
                <BsX size={24} style={{color: '#FF9292'}}/>
            </span>
        </div>
    );
}

function Unvaliable(excludeBook: Function) {
    return (
        <div className="unavaliable-card">
            <div className="d-flex align-items-center justify-content-center w-100">
                <ErrorMessage>Produto não disponível</ErrorMessage>
            </div>

            <span className="exclude" onClick={event => {
                event.stopPropagation();
                excludeBook();
            }}>
                <BsX size={24} style={{color: '#FF9292'}}/>
            </span>
        </div>
    );
}