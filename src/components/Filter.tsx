import React, { useEffect, useState } from 'react';
import api from '../services/api';

import '../styles/components/filter.css';

export default function Filter(props: any) {
    const [categories, setCategories] = useState([]);
    
    useEffect(() => {
        api.get('https://run.mocky.io/v3/70a91fc0-1dc3-4e0f-bb95-525dec4f169e').then(response => {
            setCategories(response.data.categorias);
        });
    }, []);
    
    return (
        <form id="filter" autoComplete="off">
            <label id="filterByName">
                {props.nameLabel ? props.nameLabel : 'Nome do Livro:'}
                <input type="text" autoComplete="off" placeholder="Digite o nome do livro"/>
            </label>

            <label id="filterBySort">
                Ordenar:
                <select>
                    <option>A-Z</option>
                    <option>Z-A</option>
                    <option>Quantidade (Crescente)</option>
                    <option>Quantidade (Decrescente)</option>
                    <option>Preço (Crescente)</option>
                    <option>Preço (Decrescente)</option>
                </select>
            </label>

            <label id="filterByCategory">
                Categoria:
                <select>
                    {categories.map(category => {
                        return (
                            <option key={category}>{category}</option>
                        )
                    })}
                </select>
            </label>
        </form>
    );
}
