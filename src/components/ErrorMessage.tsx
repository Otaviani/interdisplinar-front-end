import React from 'react';

import "../styles/components/error-message.css";
import advice from "../stub/advice.svg";

interface Params {
    children: any,
    soft?: boolean
}

export default function ErrorMessage(props: Params) {
    return (
        <div className="advice mb-0">
            <img className="advice-icon" src={advice} alt="Ícone de aviso"/>
            <p className={ props.soft ? 'soft-error-message' : 'error-message' }>{props.children}</p>
        </div>
    )
}