import React from 'react';
import { BsArrowDown, BsArrowUp } from 'react-icons/bs';

import '../styles/components/reports.css';

interface IReport {
    "uuid": string,
    "name": string,
    "sales": number,
    "last_month_sales": number,
    "sales_diff": number,
    "gain": number,
    "last_month_gain": number,
    "gain_diff": number,
}

export default function Report(report: IReport) {
    return (
        <div>
            <div className="col-12 d-flex align-items-center report">
                <p className="w-25">{report.name}</p>

                <div className="w-75 d-flex align-items-center justify-content-between h-100">
                    <div className="col-2 d-flex justify-content-end">
                        <span className="text-right">{report.sales}</span>
                    </div>

                    <div className="col-2 d-flex justify-content-end">
                        <span className="text-right">{report.last_month_sales}</span>
                    </div>
                    
                    <div className="col-2 d-flex justify-content-end">
                        <span className="text-right">{(report.gain).toFixed(2)}</span>
                    </div>

                    <div className="col-2 d-flex justify-content-end">
                        <span className="text-right">{(report.last_month_gain).toFixed(2)}</span>
                    </div>

                    <div className="col-2 d-flex justify-content-end">
                        <span>{report.sales_diff}</span>
                        { report.sales_diff > 0 ? <BsArrowUp size={20} style={{color: "#96e6a1", marginLeft: 8}} /> : <BsArrowDown size={20} style={{color: "#FF9292", marginLeft: 8}} /> }
                    </div>

                    <div className="col-2 d-flex justify-content-end">
                        <span>{(report.gain_diff).toFixed(2)}</span>
                        { report.gain_diff > 0 ? <BsArrowUp size={20} style={{color: "#96e6a1", marginLeft: 8}} /> : <BsArrowDown size={20} style={{color: "#FF9292", marginLeft: 8}} /> }
                    </div>
                </div>
            </div>

            <hr/>
        </div>
    );
}
