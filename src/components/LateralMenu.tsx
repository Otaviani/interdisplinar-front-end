import React from 'react';
import { Link } from 'react-router-dom';

import '../styles/components/lateral-menu.css';

import { BsBook, BsPerson, BsBarChart } from 'react-icons/bs';

interface Params {
    activePage: string
}

export default function LateralMenu(props: Params) {
    return (
        <aside id="lateral-menu" className="justify-content-center">
            <ul className="w-100">
                <Link to="/configuracoes/livros" className="d-flex align-items-center">
                    <div className={(props.activePage === 'books' ? 'lateral-active' : '') + " d-flex align-items-center w-100"}>
                        <div style={{color: 'white'}}>
                            <BsBook size={24}/>
                        </div>
                        <li>LIVROS</li>
                    </div>
                </Link>
                
                <Link to="/configuracoes/funcionarios" className="d-flex align-items-center">
                    <div className={(props.activePage === 'employees' ? 'lateral-active' : '') + " d-flex align-items-center w-100"}>
                        <div style={{color: 'white'}}>
                            <BsPerson size={24}/>
                        </div>
                        <li>FUNCIONÁRIOS</li>
                    </div>
                </Link>

                <Link to="/configuracoes/relatorios" className="d-flex align-items-center">
                    <div className={(props.activePage === 'reports' ? 'lateral-active' : '') + " d-flex align-items-center w-100"}>
                        <div style={{color: 'white'}}>
                            <BsBarChart size={24}/>
                        </div>
                        <li>RELATÓRIOS</li>
                    </div>
                </Link>
            </ul>
        </aside>
    )
}
