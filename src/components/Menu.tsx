import React from 'react';

import '../styles/components/menu.css';

import cart from '../stub/cart.svg';
import { Link } from 'react-router-dom';

import { BsGear } from 'react-icons/bs';
import logo from '../images/lila-logo.png';

import jwt_decode from "jwt-decode";
import { useSelector } from 'react-redux';

interface Params {
    active: string,
}

interface TokenDecoded {
    uuid: string,
    name: string,
    photo: string,
    is_employee: string,
}

export default function Menu(props: Params) {
    const cartItemsCount = useSelector((state: any) => state.cart.cartItems).length;
    var tokenDecoded: TokenDecoded = jwt_decode(localStorage.getItem("@library:token") ?? "");

    return (
        <div id="menu" className="d-flex align-items-center justify-content-between">
            <div className="h-100">
                <ul className="h-100">
                    <li className="h-100 d-flex align-items-center">
                        <Link to="/livros" className="h-100"><img src={logo} alt="L.I.L.A Logotipo" className="h-100" style={{pointerEvents: 'none'}}/></Link>
                    </li>
                    <li className="position-relative d-flex align-items-center h-100">
                        <Link to="/livros">Acervo</Link>
                        <span className={props.active === 'books' ? "menu-active" : ""}></span>
                    </li>
                </ul>
            </div>

            <div className="h-100">
                <ul className="h-100">
                    <li className="position-relative h-100 d-flex align-items-center">
                        <Link to="/carrinho" className="h-100 w-100">
                            <img id="cart" src={cart} alt="Carrinho"/>
                            <span className="counter">{cartItemsCount}</span>
                            <span className={props.active === 'cart' ? "menu-active" : ""}></span>
                        </Link>
                    </li>
                    {   tokenDecoded.is_employee === 'True'
                        ? (
                            <li className="position-relative h-100 d-flex align-items-center">
                                <Link to="/configuracoes/livros" className="h-100 w-100">
                                    <div style={{ color: '#050636' }}>
                                        <BsGear size={24}/>
                                        <span className={props.active === 'settings' ? "menu-active" : ""}></span>
                                    </div>
                                </Link>
                            </li>
                        ) : ''
                    }
                    
                </ul>

                <Link className="profile" to="/perfil">
                    <div>
                        Olá, &nbsp; 
                        <span>{tokenDecoded.name ?? ""}!</span>
                    </div>
                    <img id="photo" src={tokenDecoded.photo ?? ""} alt="Foto de perfil"/>
                </Link>
            </div>
        </div>
    );
}
