import React from 'react';

import '../styles/components/employee-card.css';

import { useHistory } from 'react-router-dom';
import { BsPencil } from 'react-icons/bs';

interface Employee {
    uuid: string,
    name: string,
    photo: string,
    type: string,
}

export default function EmployeeCard(employee: Employee) {
    const history = useHistory();

    function redirectToEmployeeInfo(uuid: string) {
        history.push(`/configuracoes/funcionarios/${uuid}`);
    }
    
    return (
        <div className="col-12 d-flex align-items-center employee-card" onClick={() => {redirectToEmployeeInfo(employee.uuid)}}>
            <div className="d-flex align-items-center h-100">
                <img className="employee-photo mr-3 h-100" src={employee.photo} alt="Foto do funcionário"/>
                <p>{employee.name}</p>
            </div>

            <div className="d-flex align-items-center h-100">
                <p>{employee.type}</p>

                <span className="update">
                    <BsPencil size={20}/>
                </span>
            </div>
        </div>
    );
}
