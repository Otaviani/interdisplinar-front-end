import React from 'react';

import '../styles/components/book.css';
import '../styles/components/pill-button.css';

import { useHistory } from 'react-router-dom';

interface Book {
    "uuid": string,
    "name": string,
    "author": string,
    "cover": string,
    "price": number,
    "quantity": number,
}

export default function Book(book: Book) {
    const history = useHistory();

    function redirectToBook({ uuid }: Book) {
        history.push(`/livros/${uuid}`);
    }
    
    return (
        <div className="book-separator p-2" onClick={() => {redirectToBook(book)}}>
            <div className="book col-12">
                <div className="book-head">
                    <p className="name">{book.name}</p>
                    <p className="author">{book.author}</p>
                </div>
                
                <div className="book-info">
                    {
                        book.quantity === 0 
                        ? (
                            <div className="d-flex flex-column align-items-center justify-content-center">
                                <img className="cover cover-unavailable" src={book.cover} alt={"Capa do livro " + book.name} />
                        
                                <p className="price price-unavailable">R$ {book.price.toFixed(2)}</p>
                                <button className="pill-button">Saiba Mais</button>
                            </div>
                        )
                        : (
                            <div className="d-flex flex-column align-items-center justify-content-center">
                                <img className="cover" src={book.cover} alt={"Capa do livro " + book.name} />
                        
                                <p className="price">R$ {book.price.toFixed(2)}</p>
                                <button className="pill-button">Saiba Mais</button>
                            </div>
                        )
                    }
                </div>  
            </div>
        </div>
    );
}
