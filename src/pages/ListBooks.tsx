import React, { useEffect, useState } from "react";

import '../styles/pages/list-books.css';

import api from "../services/api";

import Book from "../components/Book";
import Filter from "../components/Filter";
import Menu from "../components/Menu";

interface Book {
    "uuid": string,
    "name": string,
    "author": string,
    "cover": string,
    "price": number,
    "quantity": number,
}

export default function ListBooks() {
    const [books, setBooks] = useState<Book[]>([]);
    const [nameParam, setParamName] = useState('');
  
    useEffect(() => {
        api.get('/books', {
            params: {
                filter: nameParam
            }
        }).then(response => {
            setBooks(response.data);
        });
    }, [nameParam]);

    if (!books) {
        return <p>Carregando...</p>
    }

    return (
        <div>
            <Menu active="books"/>
        
            <div id="items-cart" className="col-12 col-lg-10 col-xl-8 mx-auto" style={{ marginTop: 64 }}>
                <form id="filter" autoComplete="off">
                    <label className="w-100">
                        Nome do Livro:
                        <input type="text" autoComplete="off" placeholder="Digite o nome do livro" value={nameParam} onChange={event => { 
                            setParamName(event.target.value) 
                        }}/>
                    </label>
                </form>

                <div className="row row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5 px-4">
                    {books.map(book => (
                        <Book key={book.uuid} {...book}/>
                    ))}
                </div>
            </div>
        </div>
    );
}