import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";

import '../styles/components/pill-button.css';
import '../styles/pages/show-book.css';

import Menu from "../components/Menu";
import Input from "../components/Input";
import Address from "../components/Address";

import { BsPersonBoundingBox, BsPlus, BsX } from "react-icons/bs";
import { FiLogOut } from "react-icons/fi";

import "../styles/pages/profile.css";
import api from "../services/api";

import jwt_decode from "jwt-decode";
import { useHistory } from "react-router-dom";

interface Address {
    uuid: string,
    state: string,
    city: string,
    cep: string,
    neighborhood: string,
    street: string,
    number: string,
    complement: string,
}

interface TokenDecoded {
    uuid: string,
    name: string,
    photo: string,
}

export default function Profile() {
    var tokenDecoded: TokenDecoded = jwt_decode(localStorage.getItem("@library:token") ?? "");

    const history = useHistory();

    const [activeHeader, setActiveHeader] = useState(0);

    const [previewPhotoUrl, setPreviewPhoto] = useState<string>('');

    const [addresses, setAddresses] = useState<Array<Address>>([]);

    const [uuid, setUuid] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [about, setAbout] = useState('');
    const [phone, setPhone] = useState('');
    const [cpf, setCpf] = useState('');
    const [photo, setPhoto] = useState<File>(null as any);

    const [cep, setCep] = useState('');
    const [state, setState] = useState('');
    const [city, setCity] = useState('');
    const [neighborhood, setNeighborhood] = useState('');
    const [street, setStreet] = useState('');
    const [number, setNumber] = useState('');
    const [complement, setComplement] = useState('');


    useEffect(() => {
        api.get('/reader').then(response => {
            setUuid(response.data.uuid);
            setName(response.data.name);
            setEmail(response.data.email);
            setAbout(response.data.about);
            setPhone(response.data.telephone);
            setCpf(response.data.cpf);
            setPreviewPhoto(response.data.photo);
        });
    }, []);

    useEffect(() => {
        api.get(`/register/via-cep/${cep}`).then(response => {
            setCep(response.data.cep);
            setCity(response.data.city);
            setNeighborhood(response.data.neighborhood);
            setState(response.data.state_address);
            setStreet(response.data.address);
        });
    }, [cep.length === 8]);

    useEffect(() => {
        api.get(`reader/${tokenDecoded.uuid}/addresses`).then(response => {
            setAddresses(response.data);
        });
    }, []);

    function handlePreviewPhoto(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setPhoto(event.target.files[0]);
        submitPhoto(event.target.files[0]);
        setPreviewPhoto(URL.createObjectURL(event.target.files[0]));
    }

    async function createAddress() {
        await api.post('/address/create', {
            cep: cep,
            state_address: state,
            city: city,
            neighborhood: neighborhood,
            address: street,
            number: number,
            complement: complement,
        });
    }

    function submitName(event: FormEvent) {
        event.preventDefault();

        api.patch(`reader/${uuid}/update-name`, {
            name: name
        }).then(response => {
            var tokenDecoded: TokenDecoded = jwt_decode(response.data.token);
            localStorage.setItem("@library:name", tokenDecoded.name);
        });
    }

    async function submitEmail() {
        await api.patch(`reader/${uuid}/update-email`, {
            email: email
        });
    }

    async function submitAbout() {
        await api.patch(`reader/${uuid}/update-description`, {
            about: about
        });
    }

    async function submitPhoto(file: File) {
        const data = new FormData();

        data.append('photoFile', file);

        await api.patch(`reader/${uuid}/update-photo`, data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        }).then(response => {
            var tokenDecoded: TokenDecoded = jwt_decode(response.data.token);
            localStorage.setItem("@library:photo", tokenDecoded.photo);
        });
    }

    async function submitNumber() {
        await api.patch(`reader/${uuid}/update-number`, {
            telephone: phone
        });
    }

    function logout() {
        localStorage.removeItem("@library:token");
        history.push('/login');
    }

    return (
        <div>
            <Menu active="profile"/>

            <section className="modal fade" id="createAddress" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content pt-4 pb-3 px-5">
                        <div className="position-absolute close close-button" data-dismiss="modal" aria-label="Close">
                            <BsX />
                        </div>

                        <div className="d-flex align-items-center justify-content-center mb-1">
                            <h4>Novo endereço</h4>
                        </div>

                        <p></p>

                        <div className="mt-5 mb-3">
                            <form>
                                <div className="d-flex">
                                    <div className="w-25 mr-3"><Input label="CEP" type="text" placeholder="CEP" value={cep} onChange={setCep} /></div>
                                    <div className="w-50 mx-3"><Input label="Cidade" type="text" placeholder="Cidade" value={city} onChange={setCity} /></div>
                                    <div className="w-25 ml-3"><Input label="Estado" type="text" placeholder="Estado" value={state} onChange={setState} /></div>
                                </div>
                                
                                <div className="d-flex my-3">
                                    <div className="w-25 mr-3"><Input label="Bairro" type="text" placeholder="Bairro" value={neighborhood} onChange={setNeighborhood} /></div>
                                    <div className="w-50 mx-3"><Input label="Logradouro" type="text" placeholder="Logradouro" value={street} onChange={setStreet} /></div>
                                    <div className="w-25 ml-3"><Input label="Numero" type="number" placeholder="Numero" value={number} onChange={setNumber} /></div>
                                </div>
                                
                                <Input label="Complemento" type="text" placeholder="Complemento" value={complement} onChange={setComplement} />
                            </form>
                        </div>

                        <div className="d-flex align-items-center justify-content-center my-4">
                            <button className="pill-button-default mx-3" data-dismiss="modal" aria-label="Close">Não, mudei de ideia</button>
                            <button onClick={createAddress} className="pill-button mx-3" data-dismiss="modal" aria-label="Close">Cadastrar Endereço</button>
                        </div>
                    </div>
                </div>
            </section>
        
            <div className="col-12 col-xl-8 mx-auto mt-5">
                <section className="pb-5" style={{height: 300}}>
                    <form className="d-flex align-items-center h-100">
                        <div>
                        {
                            previewPhotoUrl ? (
                                <label htmlFor="image" className="user-photo my-0">
                                    <img src={previewPhotoUrl} style={{objectFit: 'cover', pointerEvents: 'none', cursor: 'pointer'}} className="user-photo" alt="Foto de perfil"/>
                                </label>
                            ) : (
                                <label htmlFor="image" className="user-without-photo d-flex align-items-center justify-content-center my-0" style={{cursor: 'pointer'}}>
                                    <BsPersonBoundingBox size={32} style={{color: "#fda085"}}/>
                                </label>
                            )
                        }
                        </div>

                        <input className="d-none" type="file" id="image" accept="image/png, image/jpeg, image/bmp" onChange={event => {
                            if (event.target.value) {
                                handlePreviewPhoto(event)
                            }
                        }}/>

                        <div className="position-relative d-flex flex-column w-100 ml-4">
                            <div className="position-absolute d-flex" style={{right: 0, cursor: 'pointer'}}>
                                <FiLogOut size={24} style={{color: '#FF9292'}} onClick={logout}/>
                            </div>
                            <input className="profile-name-input w-50" value={name} onChange={(event) => setName(event.target.value)} onBlur={submitName}/>
                            <input className="profile-email-input w-50" value={email} onChange={(event) => setEmail(event.target.value)} onBlur={submitEmail}/>

                            <div className="mt-4">
                                <strong><p>Sobre mim:</p></strong>
                                <textarea className="profile-textarea w-100 h-100" placeholder="Que tal escrever algo bem legal sobre você? ;)" style={{resize: 'none'}} value={about} onChange={event => setAbout(event.target.value)} onBlur={submitAbout}/>
                            </div>
                        </div>
                    </form>
                </section>

                <section className="d-flex">
                    <div id="carouselExampleIndicators" className="carousel slide w-100" data-ride="carousel" data-interval={false} style={{ height: 475 }}>
                        <ol className="d-flex w-100 align-items-center m-0 border-bottom">
                            <li className={ (activeHeader === 0 ? "register-border-active" : "border-bottom") + " text-center py-2 px-4 active"} style={{marginBottom: -1, cursor: "pointer"}} data-target="#carouselExampleIndicators" data-slide-to="0" onClick={() => setActiveHeader(0)}>Informações Pessoais</li>
                            <li className={ (activeHeader === 1 ? "register-border-active" : "border-bottom") + " text-center py-2 px-4"} style={{marginBottom: -1, cursor: "pointer"}} data-target="#carouselExampleIndicators" data-slide-to="1" onClick={() => setActiveHeader(1)}>Compras</li>
                        </ol>

                        <div className="carousel-inner h-100">
                            <div className="carousel-item active h-100 px-3">
                                <form className="mt-4">
                                    <h4 className="mb-2"><strong>Informações Pessoais:</strong></h4>
                                    <div className="d-flex mt-3">
                                        <div className="w-25 mr-4"><Input label="CPF" type="password" placeholder="Nome" value={cpf} onChange={setCpf} disabled/></div>
                                        <div className="w-25 ml-4"><Input label="Telefone" type="text" placeholder="Telefone" value={phone} onChange={setPhone} onBlur={submitNumber}/></div>
                                    </div>
                                </form>

                                <div className="mt-4">
                                    <h4 className="mb-3"><strong>Endereços:</strong></h4>

                                    {
                                        addresses.map(address => {
                                            return (
                                                <Address key={address.uuid} {...address} />
                                            )
                                        })
                                    }

                                    <div className="w-100 d-flex justify-content-center mt-4">
                                        <button className="pill-button" type="button" data-toggle="modal" data-target="#createAddress"><BsPlus size={24}/> Novo Endereço</button>
                                    </div>
                                </div>
                            </div>

                            <div className="carousel-item h-100 px-3">

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}
