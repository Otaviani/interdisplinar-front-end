import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import ErrorMessage from "../components/ErrorMessage";
import jwt_decode from "jwt-decode";

import api from "../services/api";

import '../styles/components/pill-button.css';
import '../styles/pages/login.css';

export default function Login() {
    const history = useHistory();

    const [error, setError] = useState(false); 

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        if (localStorage.getItem("@library:token")) {
            history.push('/livros');
        }
    }, []);

    function handleLogin() {
        api.post('/login', {
            login: username,
            password: password,
        }).then(response => {
            localStorage.setItem("@library:token", `${response.data.token}`);

            history.push('/livros');
        }).catch(err => {
            setError(true);
        });
    }

    return (
        <div id="login">
            <section className="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4">
                <h1>Bem-vindo(a)!</h1>

                { (error) && (<div className="mb-3"><ErrorMessage>Login ou senha inválidos.</ErrorMessage></div>) }

                <form className="col-sm-8" onSubmit={e => {
                    e.preventDefault()
                    handleLogin()
                }}>
                    <label>
                        Email:
                        <input type="text" value={username} placeholder="Login" onChange={event => setUsername(event.target.value)}/>
                    </label>
                    
                    <label>
                        Senha:
                        <input type="password" value={password} placeholder="Senha" onChange={event => setPassword(event.target.value)}/>
                    </label>

                    <button type="submit" className="pill-button">LOGIN</button>
                </form>

                <Link to="/cadastro">CRIAR UMA CONTA</Link>

                <p>TODOS OS DIREITOS RESERVADOS. 2020.</p>
            </section>
        </div>
    );
}