import React, { useEffect, useState } from "react";

import api from "../services/api";

import '../styles/components/pill-button.css';
import '../styles/pages/show-book.css';

import Menu from "../components/Menu";
import { useHistory, useParams } from "react-router-dom";
import ErrorMessage from "../components/ErrorMessage";
import { useDispatch, useSelector } from "react-redux";
import cart from "../store/modules/cart/reducer";
import { addProductToCart } from "../store/modules/cart/actions";

interface Book {
    "uuid": string,
    "name": string,
    "synopsis": string,
    "genre": string,
    "author": string,
    "cover": string,
    "price": number,
    "quantity": number,
}

interface BookParams {
    "uuid": string,
}

export default function ShowBook() {
    const history = useHistory();
    const dispatch = useDispatch();
    const params = useParams<BookParams>();

    const [book, setBook] = useState<Book>({
        "uuid": '',
        "name": '',
        "synopsis": '',
        "genre": '',
        "author": '',
        "cover": '',
        "price": 0,
        "quantity": 0,
    });

    const [quantityPreview, setQuantityPreview] = useState(1);
    const [pricePreview, setPricePreview] = useState(0);
    const cartItems = useSelector(state => state);

    useEffect(() => {
        api.get(`books/${params.uuid}`).then(response => {
            setBook(response.data);
            setPricePreview(response.data.price);
        });
    }, []);

    function addToCart() {
        dispatch(addProductToCart({
            uuid: book.uuid,
            quantity: quantityPreview
        }));

        history.goBack();
    }

    return (
        <div>
            <Menu active="books"/>
        
            <div className="col-12 col-xl-8 mx-auto">
                <span className="hider"></span>

                <div id="show-book" className="d-flex align-items-start">
                    <img className="col-xl-3 mx-auto" src={book.cover} alt={"Capa do livro" + book.name}/>
                    
                    <div className="col-xl-6 flex-column">
                        <div className="d-flex justify-content-between">
                            <h1 className="title">{book.name}</h1>
                            <p className="genre">{book.genre}</p>
                        </div>

                        <h3 className="author">{book.author}</h3>
                        <p className="synopsis text-justify">{book.synopsis}</p>
                    </div>

                    <div className="shop-fixed col-xl-3">
                        <div className="shop-info col-12 d-flex flex-column align-items-center">
                            { displayMessage(book) }

                            <p className="price">R$ {(book.price).toFixed(2)}</p>

                            <form onSubmit={event => {
                                event.preventDefault();
                            }}>
                                <label className="label-quantity-of-books d-flex align-items-center">
                                    Quantidade:
                                    <input 
                                        className="quantity-of-books" 
                                        type="number"
                                        min="1"
                                        max={book.quantity} 
                                        value={quantityPreview}
                                        onChange={event => {
                                            setQuantityPreview(parseInt(event.target.value))
                                            setPricePreview(parseInt(event.target.value) * book.price)
                                        }}
                                    >
                                    </input>
                                </label>

                                { purchaseSummary(quantityPreview, pricePreview) }

                                <button className="pill-button" onClick={addToCart}>Adicionar ao carrinho</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

function displayMessage({ quantity }: Book) {
    return (
        <div className="book-status">
            {
                quantity === 0
                ? (
                    <ErrorMessage>Produto indisponível.</ErrorMessage>
                )
                : quantity <= 50
                    ? (
                        <div className="quantity-ending"><strong>{quantity} restante.</strong></div>
                    )
                    : (
                        <div className="quantity-available">Produto disponível!</div>
                    )
            }  
        </div>
    )
}

function purchaseSummary(quantityPreview: number, pricePreview: number) {
    return (
        <div className="purchase-summary justify-content-start">
            <p className="mb-2">Resumo da compra:</p>
        
            <div className="d-flex justify-content-between">
                <p>Quantidade</p>
                <p>Preço</p>
            </div>

            <hr className="my-2"/>

            <div className="d-flex justify-content-between">
                <p>{quantityPreview}</p>
                <p>{pricePreview.toFixed(2)}</p>
            </div>
        </div>
    )
}