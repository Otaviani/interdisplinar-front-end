import React, { useEffect, useState } from "react";
import BookCard from "../components/card/BookCard";

import '../styles/pages/config/list-books.css';
import '../styles/pages/items-cart.css';
import '../styles/components/pill-button.css';

import api from "../services/api";
import Menu from "../components/Menu";
import Footer from "../components/Footer";
import ErrorMessage from "../components/ErrorMessage";
import Filter from "../components/Filter";
import { BsBookmarkCheck, BsX } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { getBooksCart } from "../store/modules/cart/actions";
import jwt_decode from "jwt-decode";
import { Link, useHistory } from "react-router-dom";

interface TokenDecoded {
    uuid: string,
}

interface Address {
    uuid: string,
    state: string,
    city: string,
    cep: string,
    neighborhood: string,
    street: string,
    number: string,
    complement: string,
}

interface ItemsCart {
    quantity: number,
    book: Book,
}

interface Book {
    uuid: string,
    name: string,
    synopsis: string,
    genre: string,
    cover: string,
    quantity: number,
    price: number,
}

export default function ItemsCart() {
    const history = useHistory();
    const dispatch = useDispatch();

    const booksToBuy = useSelector((state: any) => state.cart.cartItems);
    const token: TokenDecoded = jwt_decode(localStorage.getItem("@library:token") ?? "");

    const [outOfStock, setOutOfStock] = useState<Book[]>([]);
    const [addresses, setAddresses] = useState<Address[]>([]);
    const [addressUuid, setAddressUuid] = useState('');

    useEffect(() => {
        dispatch(getBooksCart());
    }, [dispatch]);

    useEffect(() => {
        api.get(`reader/${token.uuid}/addresses`).then(response => {
            setAddresses(response.data);
            setAddressUuid(response.data[0].uuid ?? '')
        });
    }, []);

    const cartItems = useSelector((state: any) => state.cart.booksCart);
  
    function buy() {
        api.post('/reader/finish-purchase', {
            address_uuid: addressUuid,
            BooksToBuy: booksToBuy
        }).then(response => {
            if (response.status === 200) {
                localStorage.setItem("@library:cart", JSON.stringify([]));
            }
        });
    }

    function redirectToListBooks() {
        history.push('/livros');
    }

    return (
        <div>
            <section className="modal fade" id="paymentModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content pt-5 pb-3 px-5">
                        <div className="position-absolute close close-button" data-dismiss="modal" aria-label="Close">
                            <BsX />
                        </div>

                        <div className="d-flex align-items-center justify-content-center mb-1">
                            <h4>Finalizar compra!</h4>
                        </div>

                        <div className="my-4 overflow-auto">
                            <div className="d-flex mb-4">
                                <p>Escolha um endereço de entrega: &nbsp;&nbsp;</p>
                                <select value={addressUuid} onChange={event => setAddressUuid(event.target.value)}>
                                    {
                                        addresses.map((address: Address) => {
                                            return (
                                                <option value={address.uuid}>{address.street}, {address.number} - {address.state}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                            

                            <h5 className="mb-4"><strong>Resumo da compra:</strong></h5>
                            {
                                cartItems.map((item: ItemsCart) => {
                                    return (
                                        <div key={item.book.uuid} className="d-flex justify-content-between p-2">
                                            <div className="w-50" style={{whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis"}}>{item.book.name}</div>
                                            <div className="w-25 text-right">{item.quantity} x {(item.book.price).toFixed(2)}</div>
                                            <div className="w-25 text-right">{(item.book.price * item.quantity).toFixed(2)}</div>
                                        </div>
                                    );
                                })
                            }
                        </div>

                        <div className="d-flex align-items-center justify-content-center mb-3">
                            <p>{cartItems.length} Livros</p>
                        </div>

                        <div className="d-flex align-items-center justify-content-center my-4">
                            <button className="pill-button-default mx-3" data-dismiss="modal" aria-label="Close">Não, mudei de ideia</button>
                            <button className={ (addressUuid === '' ? "pill-button-disabled" : "pill-button") + " mx-3"} data-dismiss="modal" aria-label="Close" onClick={buy} disabled={addressUuid === ''}>Comprar</button>
                        </div>
                    </div>
                </div>
            </section>

            <Menu active="cart"/>

            <div id="static-page">
                <div className="position-relative w-100 fix-height">
                    <div className="d-flex flex-column align-items-center fix-height overflow-auto p-0">
                        <div id="books" className="col-8 d-flex flex-column">
                            {
                                cartItems.length > 0
                                    ? (
                                        cartItems.map((itemCart: ItemsCart) => {
                                            { itemCart.quantity === 0 && setOutOfStock(itemCart as any) }
                                            return (
                                                <BookCard key={itemCart.book.uuid} {...itemCart} />
                                            );
                                        })
                                    ) : (
                                        <div className="d-flex flex-column justify-content-center align-items-center">
                                            <p className="mb-4">Não conseguimos encontrar nenhum item para o seu carrinho :(</p>
                                            <button className="pill-button" onClick={() => redirectToListBooks()}>Vamos às compras!</button>
                                        </div>
                                    )
                            }
                        </div>
                    </div>

                    <Footer>
                        {
                            outOfStock.length !== 0 ? (
                                <div className="d-none d-xl-flex col-lg-8 col-xl-6 justify-content-start">
                                    <ErrorMessage><span>Existem produtos que não estão disponíveis no estoque. Ao prosseguir</span><br/><span>com a compra, os itens indisponíveis <u>não serão incluídos no pagamento</u>.</span></ErrorMessage>
                                </div>
                            ) : (
                                <div className="d-none d-xl-flex col-lg-8 col-xl-6 align-items-center justify-content-start">
                                    <BsBookmarkCheck size={20} style={{color: "#fda085", marginRight: 20}} />

                                    <div className="d-flex flex-column">
                                        <p>Uauu! Seu carrinho está cheio de grandes histórias! Para continuar </p><p>com a compra basta clicar no botão "<u>Prosseguir</u>" ao lado.</p>
                                    </div>
                                </div>
                                
                            )
                        }

                        <div className="d-none d-sm-flex col-12 col-lg-4 col-xl-6 justify-content-sm-center justify-content-lg-end">
                            <button className="pill-button" type="button" data-toggle="modal" data-target="#paymentModal">Prosseguir</button>
                        </div>
                    
                        <div className="col-12 d-sm-none p-0 h-100">
                            <button className="col-12 full-button" type="button" data-toggle="modal" data-target="#paymentModal">Prosseguir</button>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}
