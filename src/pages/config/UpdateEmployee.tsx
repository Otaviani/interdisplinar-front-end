import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import Footer from "../../components/Footer";
import { BsPersonBoundingBox, BsPersonPlus, BsX } from "react-icons/bs";

import '../../styles/helpers.css';
import '../../styles/pages/config/admission.css';
import Input from "../../components/Input";
import api from "../../services/api";
import { useHistory, useParams } from "react-router-dom";
import ErrorMessage from "../../components/ErrorMessage";

interface Employee {
    name: string,
    cpf: string,
    role: string,
    email: string,
    phone: string,
    birth: Date,
    photo: File,
}

interface EmployeeParams {
    uuid: string,
}

export default function UpdateEmployee() {
    const history = useHistory();
    const [previewPhoto, setPreviewPhoto] = useState<string>('');
    const params = useParams<EmployeeParams>();

    const [uuid, setUuid] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [cpf, setCpf] = useState<string>('');
    const [role, setRole] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [birth, setBirth] = useState<string>('');
    const [photo, setPhoto] = useState<File>(null as any);
    
    useEffect(() => {
        api.get(`/settings/employees/${params.uuid}`).then(response => {
            setUuid(response.data.uuid);
            setName(response.data.name);
            setCpf(response.data.cpf);
            setRole(response.data.employee_type);
            setEmail(response.data.email);
            setPhone(response.data.phone);
            setBirth(response.data.birth);
            setPreviewPhoto(response.data.photo);
        });
    }, []);

    function handlePreviewPhoto(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setPhoto(event.target.files[0]);
        setPreviewPhoto(URL.createObjectURL(event.target.files[0]));
    }

    function removePreviewCoverUrl() {
        setPreviewPhoto('');
    }

    async function updateEmployee(event: FormEvent) {
        event.preventDefault();
        const data = new FormData();

        data.append('name', name);
        data.append('cpf', cpf);
        data.append('employee_type', role);
        data.append('email', email);
        data.append('telephone', phone);
        data.append('birth', birth);
        data.append('photoFile', photo);

        await api.put(`/settings/employees/${params.uuid}/update`, data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        });

        history.push('/configuracoes/funcionarios');
    }

    async function fireEmployee() {
        await api.delete(`/settings/employees/${uuid}/delete`);

        history.push('/configuracoes/funcionarios');
    }

    return (
        <div>
            <Menu active="settings"/>

            <section className="modal fade" id="fireEmployee" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content pt-4 pb-3 px-5">
                        <div className="position-absolute close close-button" data-dismiss="modal" aria-label="Close">
                            <BsX />
                        </div>

                        <div className="d-flex align-items-center justify-content-center mb-1">
                            <h4>Você tem certeza?</h4>
                        </div>

                        <form onSubmit={event => {
                            event.preventDefault();
                        }} className="d-flex align-items-center justify-content-center my-4">
                            <button className="pill-button-default mx-3" data-dismiss="modal" aria-label="Close">Não, mudei de ideia</button>
                            <button onClick={() => fireEmployee()} className="pill-button-danger mx-3" data-dismiss="modal" aria-label="Close">Sim, Demitir</button>
                        </form>
                    </div>
                </div>
            </section>

            <div id="static-page">
                <LateralMenu activePage="employees"/>

                <div className="position-relative w-100">
                    <div className="d-flex flex-column align-items-center fix-height p-0">
                        <section id="books" className="col-10 d-flex flex-column">
                            <header className="d-flex">
                                <h2 className="w-75">Editar Funcionário</h2>

                                <div className="d-flex w-50 justify-content-end">
                                    <ErrorMessage soft><strong>Demitir funcionário.</strong></ErrorMessage>
                                    <button className="pill-button-default ml-4" data-toggle="modal" data-target="#fireEmployee">Demitir</button>
                                </div>
                            </header>

                            <section className="d-flex flex-column col-12 p-0">
                                <div className="mt-4">
                                    <h5>1. Preencha os dados do novo colaborador!</h5>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-5 p-0 pr-3"><Input label="Nome" placeholder="Nome" value={name} onChange={setName}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="CPF" placeholder="CPF" value={cpf} onChange={setCpf}/></div>
                                        <div className="col-12 col-lg-3 p-0 pl-3"><Input label="Cargo" placeholder="Cargo" value={role} onChange={setRole}/></div>
                                    </div>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-5 d-flex justify-content-end p-0 pr-3">
                                            <Input label="E-mail" placeholder="E-mail" value={email} onChange={setEmail}/>
                                            <p className="position-absolute"><small>O email será usado para o login do funcionário!</small></p>
                                        </div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="Telefone" placeholder="Telefone" value={phone} onChange={setPhone}/></div>
                                        <div className="col-12 col-lg-3 p-0 px-3"><Input label="Data de Nascimento" value={birth} placeholder="Data de Nascimento" onChange={setBirth}/></div>
                                    </div>
                                </div>
                                
                                <div className="mt-4">
                                    <h5 className="mb-5">2. Agora, vamos inserir uma foto!</h5>

                                    <div className="d-flex justify-content-center">
                                        {
                                            previewPhoto ? (
                                                <img src={previewPhoto} style={{objectFit: 'cover', pointerEvents: 'none'}} alt="Foto do funcionário" className="photo mb-4"/>
                                            ) : (
                                                <label htmlFor="image[]" style={{cursor: 'pointer'}} className="photo without-photo d-flex align-items-center justify-content-center mb-4">
                                                    <BsPersonBoundingBox size={32} style={{color: "#fda085"}}/>
                                                </label>
                                            )
                                        }
                                    </div>

                                    {
                                        previewPhoto ? (
                                            <div className="d-flex justify-content-center mx-auto w-50">
                                                <button className="pill-button-default mx-2 my-3 w-25" onClick={removePreviewCoverUrl}>Cancelar envio</button>
                                                <label htmlFor="image[]" className="pill-button mx-2 my-3 w-25 d-flex align-items-center justify-content-center">
                                                    Enviar Foto
                                                </label>
                                            </div>
                                        ) : (
                                            <div className="d-flex justify-content-center mx-auto w-50">
                                                <label htmlFor="image[]" className="pill-button mx-2 my-3 w-25 d-flex align-items-center justify-content-center py-2">
                                                    Enviar Foto
                                                </label>
                                            </div>
                                        )
                                    }

                                    <input className="d-none" type="file" id="image[]" accept="image/png, image/jpeg, image/bmp"onChange={event => {
                                        if (event.target.value) {
                                            handlePreviewPhoto(event)
                                        }
                                    }}/>
                                </div>
                            </section>
                        </section>
                    </div>

                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsPersonPlus size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>

                            <div className="d-flex flex-column">
                                <p>Você está quase acabando, basta preencher</p>
                                <p>todos os dados e confirmar o cadastro.</p>
                            </div>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <form method="put" onSubmit={updateEmployee}>
                                <button type="submit" id="new-book" className="pill-button">Editar Funcionário</button>
                            </form>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}