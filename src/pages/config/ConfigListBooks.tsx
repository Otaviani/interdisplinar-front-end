import React, { useEffect, useState } from "react";

import '../../styles/pages/list-books.css';
import '../../styles/components/pill-button.css';

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import BookAdminCard from "../../components/card/BookAdminCard";
import Filter from "../../components/Filter";
import Footer from "../../components/Footer";
import { BsChatQuote } from "react-icons/bs";
import api from "../../services/api";
import { Link } from "react-router-dom";

interface Book {
    "uuid": string,
    "isbn": string,
    "name": string,
    "synopsis": string,
    "genre": string,
    "cover": string,
    "quantity": number,
    "price": number,
}

export default function ConfigListBooks() {
    const [books, setBooks] = useState<Book[]>([]);
  
    useEffect(() => {
        api.get('/settings/books').then(response => {
            setBooks(response.data);
        });
    }, []);

    return (
        <div>
            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="books"/>

                <div className="position-relative w-100 fix-height">
                    <div className="d-flex flex-column align-items-center fix-height overflow-auto p-0">
                        <div id="books" className="col-10 d-flex flex-column">
                            <Filter nameLabel="Nome do Livro ou ISBN:"/>
                            {
                                books.length ?
                                    books.map(book => {
                                        return (
                                            <div>
                                                <BookAdminCard key={book.uuid} {...book}/>
                                            </div>
                                        );
                                    }) : (
                                    <div>
                                        <p>Ooh! Não conseguimos encontrar nenhum livro! <span role="img" aria-label="emoji triste">😥</span></p>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    
                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsChatQuote size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>
                            <p>Nesta página, você pode ver seus livros e gerencia-los!</p>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <p>{ books.length } livros</p>
                            <span className="vertical-line"/>
                            <Link to="/configuracoes/livros/cadastrar-livro"><button id="new-book" className="pill-button">Novo Livro</button></Link>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}