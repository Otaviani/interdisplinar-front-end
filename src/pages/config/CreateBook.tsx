import React, { ChangeEvent, FormEvent, useState } from "react";

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import Footer from "../../components/Footer";
import { BsBookmarkPlus, BsImage, BsPlus } from "react-icons/bs";
import * as yup from 'yup';

import '../../styles/helpers.css';
import Input from "../../components/Input";
import TextArea from "../../components/TextArea";
import { useHistory } from "react-router-dom";
import api from "../../services/api";

interface Book {
    name: string,
    isbn: string,
    pages: number,
    author: string,
    publishingCompany: string,
    genre: string,
    synopsis: string,
    price: number,
    quantity: number,
    cover: File,
}

export default function CreateBook() {
    const history = useHistory();

    const [previewIsbn, setPreviewIsbn] = useState<string>('');
    const [previewCoverUrl, setPreviewCover] = useState<string>('');

    const [name, setName] = useState<string>('');
    const [isbn, setIsbn] = useState<string>('');
    const [pages, setPages] = useState<number>(0);
    const [author, setAuthor] = useState<string>('');
    const [publishingCompany, setPublishingCompany] = useState<string>('');
    const [gender, setGender] = useState<string>('');
    const [synopsis, setSynopsis] = useState<string>('');
    const [price, setPrice] = useState<number>(0);
    const [quantity, setQuantity] = useState<number>(0);
    const [cover, setCover] = useState<File>(null as any);

    function handlePreviewCover(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setCover(event.target.files[0]);
        setPreviewCover(URL.createObjectURL(event.target.files[0]));
    }

    function removePreviewCoverUrl() {
        setPreviewCover('');
    }

    async function submitBookCreation(event: FormEvent) {
        event.preventDefault();

        const book = instanceBook();
        validate(book);

        const data = new FormData();

        data.append('name', name);
        data.append('isbn', isbn);
        data.append('pages', String(pages));
        data.append('author', author);
        data.append('publishing_company', publishingCompany);
        data.append('genre', gender);
        data.append('synopsis', synopsis);
        data.append('price', String(price));
        data.append('quantity', String(quantity));
        data.append('coverFile', cover);

        await api.post('/settings/books/create', data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        });
        
        history.push('/configuracoes/livros');
    }

    function instanceBook() {
        let book: Book = {
            name: name,
            isbn: isbn,
            pages: pages,
            author: author,
            publishingCompany: publishingCompany,
            genre: gender,
            synopsis: synopsis,
            price: price,
            quantity: quantity,
            cover: cover,
        };

        return book;
    }

    function validate(data: Book) {
        const schema = yup.object().shape({
            name: yup.string().min(3).required(),
            isbn: yup.string().required(),
            pages: yup.number().integer().required(),
            author: yup.string().required(),
            publishingCompany: yup.string().required(),
            genre: yup.string().required(),
            synopsis: yup.string().required(),
            price: yup.number().required(),
            quantity: yup.number().min(1).integer().required(),
        });

        schema.validate(data, {
            abortEarly: false,
        });
    }

    function searchBookByAPI(isbn: string) {
        api.post('settings/find-book', {
            isbn: isbn
        }).then(response => {
            if (!response.data) {
            } else {
                setIsbn(response.data.isbn);
                setName(response.data.name);
                setPages(response.data.pages);
                setAuthor(response.data.author);
                setPublishingCompany(response.data.publishing_company);
                setSynopsis(response.data.synopsis);
            }
        });
    }

    return (
        <div>
            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="books"/>

                <div className="position-relative w-100">
                    <div className="d-flex flex-column align-items-center fix-height p-0">
                        <section id="books" className="col-10 d-flex flex-column">
                            <header className="mb-4 h-100">
                                <h2 className="mb-3">Cadastrar Livro</h2>
        
                                <div className="d-flex h-50 align-items-center">
                                    <div className="col-12 col-xl-6 p-0">
                                        <p>Você pode consultar o ISBN do livro que você quer cadastrar para preencher os campos automaticamente! Ou então, optar por preencher os campos.</p>
                                    </div>

                                    <div className="col-1 d-flex align-items-center h-100">
                                        <span className="vertical-line"></span>
                                    </div>
                                    
                                    <form onSubmit={(event) => {
                                        event.preventDefault();
                                        searchBookByAPI(previewIsbn);
                                    }} className="d-flex align-items-center justify-content-between col-12 col-xl-5">
                                        <div className="p-0 pr-4 w-50"><Input label="ISBN" placeholder="ISBN" value={previewIsbn} onChange={setPreviewIsbn}/></div>
                                        <div className="p-0 w-50"><button type="submit" className="pill-button w-100">Buscar Livro</button></div>
                                    </form>
                                </div>
                            </header>

                            <section className="d-flex col-12 p-0">
                                <aside className="d-flex flex-column align-items-center col-4">
                                    <div>
                                        {
                                            previewCoverUrl ? (
                                                <img src={previewCoverUrl} style={{objectFit: 'cover', pointerEvents: 'none'}} alt="Capa do Livro" width="320" height="480" className="mb-1"/>
                                            ) : (
                                                <label htmlFor="image[]" style={{width: 320, height: 480, cursor: 'pointer'}} className="without-cover d-flex align-items-center justify-content-center mb-1">
                                                    <BsImage size={32} style={{color: "#fda085"}}/>
                                                    <BsPlus size={24} style={{color: "#fda085", position: "absolute", marginRight: -38, marginTop: -35}}/>
                                                </label>
                                            )
                                        }
                                    </div>

                                    <p className="text-small text-center"><small>Tamanho de imagem mínima recomendada: 320 x 480</small></p>
                                    <div className="d-flex w-100">
                                        {
                                            previewCoverUrl ? (
                                                <div className="d-flex w-100">
                                                    <button className="pill-button-default mx-2 my-3 w-50" onClick={removePreviewCoverUrl}>Cancelar envio</button>

                                                    <label htmlFor="image[]" className="pill-button mx-2 my-3 w-50 d-flex align-items-center justify-content-center">
                                                        Enviar Foto
                                                    </label>
                                                </div>
                                            ) : (
                                                <div className="d-flex justify-content-center w-100">
                                                    <label htmlFor="image[]" className="pill-button mx-2 my-3 w-50 d-flex align-items-center justify-content-center py-2">
                                                        Enviar Foto
                                                    </label>
                                                </div>
                                            )
                                        }
                                        
                                        <input className="d-none" type="file" id="image[]" accept="image/png, image/jpeg, image/bmp" onChange={event => {
                                            if (event.target.value) {
                                                handlePreviewCover(event)
                                            }
                                        }}/>
                                    </div>
                                </aside>

                                <div className="d-flex flex-column col-8">
                                    <h3>Informações do Livro</h3>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-4 p-0 pr-3"><Input label="Nome" placeholder="Nome do Livro" value={name} onChange={setName}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="ISBN" placeholder="ISBN" value={isbn} onChange={setIsbn}/></div>
                                        <div className="col-12 col-lg-4 p-0 pl-3"><Input label="Páginas" type="number" placeholder="Quantidade de Páginas" value={pages} onChange={setPages}/></div>
                                    </div>

                                    <div className="d-flex justify-content-between p-0 mt-4 mb-4">
                                        <div className="col-12 col-lg-4 p-0 pr-3"><Input label="Autor" placeholder="Nome do Autor" value={author} onChange={setAuthor}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="Editora" placeholder="Nome da Editora" value={publishingCompany} onChange={setPublishingCompany}/></div>
                                        <div className="col-12 col-lg-4 p-0 pl-3"><Input label="Gênero" placeholder="Gênero" value={gender} onChange={setGender}/></div>
                                    </div>

                                    <TextArea label="Sinopse" rows={5} value={synopsis} onChange={setSynopsis}/>

                                    <hr/>
                                    <h3>Informações do Estoque</h3>

                                    <div className="d-flex p-0 mt-4 mb-4">
                                        <div className="col-12 col-lg-6 p-0 pr-3"><Input label="Preço" type="number" placeholder="R$ 00,00" value={price} onChange={setPrice}/></div>
                                        <div className="col-12 col-lg-6 p-0 pl-3"><Input label="Quantidade em Estoque" placeholder="Unidades" value={quantity} onChange={setQuantity}/></div>
                                    </div>
                                </div>
                            </section>
                        </section>
                    </div>

                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsBookmarkPlus size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>

                            <div className="d-flex flex-column">
                                <p>Para cadastrar um livro, basta preencher os campos ou solicitar</p>
                                <p>o preenchimento automático, e clicar em "<u>Cadastrar Livro</u>".</p>
                            </div>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <form method="post" onSubmit={submitBookCreation}>
                                <button type="submit" id="new-book" className="pill-button">Cadastrar Livro</button>
                            </form>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}