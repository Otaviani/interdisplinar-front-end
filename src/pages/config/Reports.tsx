import React, { useEffect, useState } from "react";

import '../../styles/components/pill-button.css';

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import Filter from "../../components/Filter";
import api from "../../services/api";
import Report from "../../components/Report";
interface IReport {
    "uuid": string,
    "name": string,
    "sales": number,
    "last_month_sales": number,
    "sales_diff": number,
    "gain": number,
    "last_month_gain": number,
    "gain_diff": number,
}

export default function Reports() {
    const [soldBooks, setSoldBooks] = useState(0);
    const [revenue, setRevenue] = useState(0);
    const [lastMonthRevenue, setLasMonthRevenue] = useState(0);
    const [newBooks, setNewBooks] = useState(0);

    const [reports, setReports] = useState<IReport[]>([]);
  
    useEffect(() => {
        api.get('/settings/reports').then(response => {
            setSoldBooks(response.data.sold_books);
            setRevenue(response.data.revenue);
            setLasMonthRevenue(response.data.last_month_revenue);
            setNewBooks(response.data.new_books);

            setReports(response.data.reports);
        });
    }, []);

    return (
        <div>
            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="reports"/>

                <div className="position-relative w-100 fix-height overflow-auto">
                    <div className="d-flex flex-column align-items-center p-0">
                        <section style={{marginTop: 64}} className="col-10 d-flex flex-column fix-height">
                            <h3 className="mb-4">Reletório mensal</h3>

                            { Status(soldBooks, revenue, lastMonthRevenue, newBooks) }

                            <Filter nameLabel="Nome do Livro ou ISBN:"/>

                            { TableHeader() }

                            {
                                reports.length ?
                                    reports.map(report => {
                                        return (
                                            <Report key={report.uuid} {...report}/>
                                        );
                                    }) : (
                                    <div className="d-flex flex-column align-items-center">
                                        <p>Ooh! Não conseguimos montar um relatório! <span role="img" aria-label="emoji triste">😥</span></p>
                                        <p>Volte novamente quando sua primeira venda for feita!</p>
                                    </div>
                                )
                            }
                        </section>
                    </div>
                </div>
            </div>
        </div>
    );
}

function TableHeader() {
    return (
        <div className="sticky-report-menu col-12 d-flex align-items-center report mb-3 pt-5 pb-4 h-100">
            <div className="w-25 d-flex align-items-start h-100">
                <span className="description">LIVRO</span>
            </div>

            <div className="w-75 d-flex align-items-start justify-content-between h-100">
                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">VENDAS (un)</span>
                    <span className="annotation">MÊS ATUAL</span>
                </div>

                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">VENDAS (un)</span>
                    <span className="annotation">MÊS PASSADO</span>
                </div>

                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">RECEITA (R$)</span>
                    <span className="annotation">MÊS ATUAL</span>
                </div>
            
                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">RECEITA (R$)</span>
                    <span className="annotation">MÊS PASSADO</span>
                </div>

                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">VENDAS (un)</span>
                    <span className="annotation">COMPARAÇÃO</span>
                </div>

                <div className="col-2 d-flex flex-column justify-content-end">
                    <span className="description">RECEITA (R$)</span>
                    <span className="annotation">COMPARAÇÃO</span>

                </div>
            </div>
        </div>
    )
}

function Status(soldBooks: number, revenue: number, lastMonthRevenue: number, newBooks: number) {
    return (
        <section className="col-10 d-flex h-auto mt-4 mb-5 mx-auto">
            <div className="col-4">
                <div className="report-box col-12 d-flex flex-column align-items-center justify-content-center py-5 h-100">
                    <h4 className="mb-3">Livros vendidos</h4>
                    <span>{soldBooks} exemplares.</span>
                </div>
            </div>

            <div className="col-4">
                <div className="report-box col-12 d-flex flex-column align-items-center justify-content-center py-5 h-100">
                    <h4 className="mb-3">Vendas (R$)</h4>
                    <div className="d-flex">
                        <div className="d-flex flex-column">
                            <span>R$ {revenue.toFixed(2)}</span>
                            <span className="text-secondary mt-2"><small>R$ {lastMonthRevenue.toFixed(2)} (No mês anterior).</small></span>
                        </div>
                    </div>
                </div>

            </div>

            <div className="col-4 d-flex">
                <div className="report-box col-12 d-flex flex-column align-items-center justify-content-center py-5 h-100">
                    <h4 className="mb-3">Novos Livros</h4>
                    <span>{newBooks ?? 0} livros.</span>
                </div>
            </div>
        </section>
    );
}