import React, { useEffect, useState } from "react";

import '../../styles/pages/list-books.css';
import '../../styles/components/pill-button.css';

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import EmployeeCard from "../../components/EmployeeCard";
import Filter from "../../components/Filter";
import Footer from "../../components/Footer";
import { BsPersonLinesFill } from "react-icons/bs";
import api from "../../services/api";
import { useHistory } from "react-router-dom";

interface Employee {
    uuid: string,
    name: string,
    photo: string,
    type: string,
}

export default function ConfigEmployees() {
    const history = useHistory();

    const [employees, setEmployees] = useState<Employee[]>([]);
  
    useEffect(() => {
        api.get('/settings/employees').then(response => {
            setEmployees(response.data);
        });
    }, []);

    function redirectToEmployeeAdmission() {
        history.push('/configuracoes/funcionarios/cadastrar-funcionario');
    }

    return (
        <div>
            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="employees"/>

                <div className="position-relative w-100 fix-height">
                    <div className="d-flex flex-column align-items-center fix-height overflow-auto p-0">
                        <div id="books" className="col-10 d-flex flex-column">
                            <Filter nameLabel="Nome do Livro ou ISBN:"/>
                            {
                                employees.length ?
                                    employees.map(employee => {
                                        return (
                                            <div>
                                                <EmployeeCard key={employee.uuid} {...employee}/>
                                            </div>
                                        );
                                    }) : (
                                    <div className="d-flex flex-column align-items-center">
                                        <p>Ooh! Não conseguimos encontrar nenhum funcionário! <span role="img" aria-label="sad">😥</span></p>
                                        <p>Que tal iniciar uma nova admissão?</p>
                                    </div>
                                )
                            }
                        </div>
                    </div>

                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsPersonLinesFill size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>
                            <p>Gerencie os funcionários da sua empresa!</p>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <p>{ 
                                employees.length <= 1 ? (
                                    <p>{employees.length} funcionário</p>
                                ) : (
                                    <p>{employees.length} funcionários</p>
                                )
                            } </p>
                            <span className="vertical-line"/>
                            <form onSubmit={event => {
                                redirectToEmployeeAdmission();
                            }}>
                                <button type="submit" id="new-book" className="pill-button">Admitir Funcionário</button>
                            </form>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}