import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import Footer from "../../components/Footer";
import { BsPersonBoundingBox, BsPersonPlus } from "react-icons/bs";

import '../../styles/helpers.css';
import '../../styles/pages/config/admission.css';
import Input from "../../components/Input";
import { useHistory } from "react-router-dom";
import api from "../../services/api";

interface Employee {
    name: string,
    cpf: string,
    role: string,
    email: string,
    phone: string,
    birth: Date,
    photo: File,
}

export default function Admission() {
    const history = useHistory();

    const [previewPhoto, setPreviewPhoto] = useState<string>('');

    const [name, setName] = useState<string>('');
    const [cpf, setCpf] = useState<string>('');
    const [role, setRole] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [birth, setBirth] = useState<string>('');
    const [photo, setPhoto] = useState<File>(null as any);

    const [cep, setCep] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [street, setStreet] = useState('');
    const [neighborhood, setNeighborhood] = useState('');
    const [number, setNumber] = useState('');
    const [complement, setComplement] = useState('');

    useEffect(() => {
        api.get(`/register/via-cep/${cep}`).then(response => {
            setCep(response.data.cep);
            setCity(response.data.city);
            setNeighborhood(response.data.neighborhood);
            setState(response.data.state_address);
            setStreet(response.data.address);
        });
    }, [cep.length === 8]);
    
    async function admitEmployee(event: FormEvent) {
        event.preventDefault();

        const data = new FormData();

        data.append('name', name);
        data.append('cpf', cpf);
        data.append('role', role);
        data.append('email', email);
        data.append('telephone', phone);
        data.append('birth', birth);
        data.append('photoFile', photo);
        data.append('employee_type', role);
        data.append('password', '123'); // Alterar depois

        data.append('cep', cep);
        data.append('city', city);
        data.append('state', state);
        data.append('street', street);
        data.append('neighborhood', neighborhood);
        data.append('number', number);
        data.append('complement', complement);

        await api.post('/settings/employee/create', data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        });
        
        history.push('/configuracoes/funcionarios');
    }

    function handlePreviewPhoto(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setPhoto(event.target.files[0]);
        setPreviewPhoto(URL.createObjectURL(event.target.files[0]));
    }

    function removePreviewCoverUrl() {
        setPreviewPhoto('');
    }

    return (
        <div>
            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="employees"/>

                <div className="position-relative w-100">
                    <div className="d-flex flex-column align-items-center fix-height p-0 overflow-auto">
                        <section id="books" className="col-10 d-flex flex-column">
                            <header>
                                <h2>Admissão de funcionário</h2>
                            </header>

                            <section className="d-flex flex-column col-12 p-0">
                                <div className="mt-4">
                                    <h5>1. Preencha os dados do novo colaborador!</h5>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-5 p-0 pr-3"><Input label="Nome" placeholder="Nome" value={name} onChange={setName}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="CPF" placeholder="CPF" value={cpf} onChange={setCpf}/></div>
                                        <div className="col-12 col-lg-3 p-0 pl-3"><Input label="Cargo" placeholder="Cargo" value={role} onChange={setRole}/></div>
                                    </div>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-5 d-flex justify-content-end p-0 pr-3">
                                            <Input label="E-mail" placeholder="E-mail" value={email} onChange={setEmail}/>
                                            <p className="position-absolute"><small>O email será usado para o login do funcionário!</small></p>
                                        </div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="Telefone" placeholder="Telefone" value={phone} onChange={setPhone}/></div>
                                        <div className="col-12 col-lg-3 p-0 pl-3"><Input label="Data de Nascimento" value={birth} placeholder="Data de Nascimento" onChange={setBirth}/></div>
                                    </div>
                                </div>

                                <div className="mt-4">
                                    <h5>2. Vamos inserir ao menos um endereço!</h5>

                                    <div className="d-flex p-0 mt-4 p-0">
                                        <div className="col-12 col-lg-2"><Input label="CEP" type="text" placeholder="CEP" value={cep} onChange={setCep}/></div>
                                        <div className="col-12 col-lg-1"><Input label="Estado" type="text" placeholder="Estado" value={state} onChange={setState}/></div>
                                        <div className="col-12 col-lg-2"><Input label="Cidade" type="text" placeholder="Cidade" value={city} onChange={setCity}/></div>
                                        <div className="col-12 col-lg-3"><Input label="Bairro" type="text" placeholder="Bairro" value={neighborhood} onChange={setNeighborhood}/></div>
                                        <div className="col-12 col-lg-3"><Input label="Logradouro" type="text" placeholder="Logradouro" value={street} onChange={setStreet}/></div>
                                        <div className="col-12 col-lg-1"><Input label="Numero" type="text" placeholder="Numero" value={number} onChange={setNumber}/></div>
                                    </div>

                                    <div className="d-flex p-0 mt-4 p-0">
                                        <div className="col-12"><Input label="Complemento" type="text" placeholder="Complemento" value={complement} onChange={setComplement}/></div>
                                    </div>
                                </div>
                                
                                <div className="mt-4">
                                    <h5 className="mb-5">3. Agora, vamos inserir uma foto!</h5>

                                    <div className="d-flex justify-content-center">
                                        {
                                            previewPhoto ? (
                                                <img src={previewPhoto} style={{objectFit: 'cover', pointerEvents: 'none'}} alt="Foto do funcionário" className="photo mb-4"/>
                                            ) : (
                                                <label htmlFor="image[]" style={{cursor: 'pointer'}} className="photo without-photo d-flex align-items-center justify-content-center mb-4">
                                                    <BsPersonBoundingBox size={32} style={{color: "#fda085"}}/>
                                                </label>
                                            )
                                        }
                                    </div>

                                    {
                                        previewPhoto ? (
                                            <div className="d-flex justify-content-center mx-auto w-50">
                                                <button className="pill-button-default mx-2 my-3 w-25" onClick={removePreviewCoverUrl}>Cancelar envio</button>
                                                <label htmlFor="image[]" className="pill-button mx-2 my-3 w-25 d-flex align-items-center justify-content-center">
                                                    Enviar Foto
                                                </label>
                                            </div>
                                        ) : (
                                            <div className="d-flex justify-content-center mx-auto w-50">
                                                <label htmlFor="image[]" className="pill-button mx-2 my-3 w-25 d-flex align-items-center justify-content-center py-2">
                                                    Enviar Foto
                                                </label>
                                            </div>
                                        )
                                    }

                                    <input className="d-none" type="file" id="image[]" accept="image/png, image/jpeg, image/bmp" onChange={event => {
                                        if (event.target.value) {
                                            handlePreviewPhoto(event)
                                        }
                                    }}/>
                                </div>
                            </section>
                        </section>
                    </div>

                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsPersonPlus size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>

                            <div className="d-flex flex-column">
                                <p>Você está quase acabando, basta preencher</p>
                                <p>todos os dados e confirmar o cadastro.</p>
                            </div>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <form method="post" onSubmit={admitEmployee}>
                                <button type="submit" id="new-book" className="pill-button">Admitir Funcionário</button>
                            </form>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );
}