import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";

import Menu from "../../components/Menu";
import LateralMenu from "../../components/LateralMenu";
import Footer from "../../components/Footer";
import { BsImage, BsPencilSquare, BsPlus, BsX } from "react-icons/bs";
import * as yup from 'yup';

import '../../styles/helpers.css';
import Input from "../../components/Input";
import TextArea from "../../components/TextArea";
import { useHistory, useParams } from "react-router-dom";
import api from "../../services/api";
import Book from "../../components/Book";
import ErrorMessage from "../../components/ErrorMessage";

interface Book {
    uuid: string,
    name: string,
    isbn: string,
    pages: number,
    author: string,
    publishingCompany: string,
    genre: string,
    synopsis: string,
    price: number,
    quantity: number,
    cover: File,
}

interface BookParams {
    uuid: string,
}

export default function UpdateBook() {
    const params = useParams<BookParams>();

    const history = useHistory();

    const [previewCoverUrl, setPreviewCover] = useState<string>('');

    const [uuid, setUuid] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [isbn, setIsbn] = useState<string>('');
    const [pages, setPages] = useState<number>(0);
    const [author, setAuthor] = useState<string>('');
    const [publishingCompany, setPublishingCompany] = useState<string>('');
    const [genre, setGenre] = useState<string>('');
    const [synopsis, setSynopsis] = useState<string>('');
    const [price, setPrice] = useState<number>(0);
    const [quantity, setQuantity] = useState<number>(0);
    const [cover, setCover] = useState<File>(null as any);

    useEffect(() => {
        api.get(`books/${params.uuid}`).then(response => {
            setUuid(response.data.uuid);
            setName(response.data.name);
            setIsbn(response.data.isbn);
            setPages(response.data.pages);
            setAuthor(response.data.author);
            setPublishingCompany(response.data.publishing_company);
            setGenre(response.data.genre);
            setSynopsis(response.data.synopsis);
            setPrice(response.data.price);
            setQuantity(response.data.quantity);
            setPreviewCover(response.data.cover);
        });
    }, []);

    function handlePreviewCover(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setCover(event.target.files[0]);
        setPreviewCover(URL.createObjectURL(event.target.files[0]));
    }

    function removePreviewCoverUrl() {
        setPreviewCover('');
    }

    async function submitBookCreation(event: FormEvent) {
        event.preventDefault();

        const book = instanceBook();
        validate(book);

        const data = new FormData();

        data.append('name', name);
        data.append('isbn', isbn);
        data.append('pages', String(pages));
        data.append('author', author);
        data.append('publishing_company', publishingCompany);
        data.append('genre', genre);
        data.append('synopsis', synopsis);
        data.append('price', String(price));
        data.append('quantity', String(quantity));
        data.append('coverFile', cover);

        await api.put(`/settings/books/${uuid}/update`, data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        });
        
        history.push('/configuracoes/livros');
    }

    async function submitBookDestroy() {
        await api.delete(`/settings/books/${uuid}/delete`);

        history.push('/configuracoes/livros');
    }

    function instanceBook() {
        let book: Book = {
            uuid: uuid,
            name: name,
            isbn: isbn as any,
            pages: pages,
            author: author,
            publishingCompany: publishingCompany,
            genre: genre,
            synopsis: synopsis,
            price: price,
            quantity: quantity,
            cover: cover,
        };

        return book;
    }

    function validate(data: Book) {
        const schema = yup.object().shape({
            name: yup.string().min(3).required(),
            pages: yup.number().integer().required(),
            author: yup.string().required(),
            publishingCompany: yup.string().required(),
            genre: yup.string().required(),
            synopsis: yup.string().required(),
            price: yup.number().required(),
            quantity: yup.number().min(1).integer().required(),
        });

        schema.validate(data, {
            abortEarly: false,
        });
    }

    return (
        <div>
            { removeBookModal(instanceBook(), previewCoverUrl) }

            <Menu active="settings"/>

            <div id="static-page">
                <LateralMenu activePage="books"/>

                <div className="position-relative w-100">
                    <div className="d-flex flex-column align-items-center fix-height p-0">
                        <section id="books" className="col-10 d-flex flex-column">
                            <header className="mb-5 h-100">
                                <h2 className="mb-3">Cadastrar Livro</h2>
        
                                <div className="d-flex h-50 align-items-center">
                                    <div className="col-12 col-xl-6 p-0">
                                        <p>Você pode consultar o ISBN do livro que você quer cadastrar para preencher os campos automaticamente! Ou então, optar por preencher os campos.</p>
                                    </div>

                                    <div className="col-1 d-flex align-items-center h-100">
                                        <span className="vertical-line"></span>
                                    </div>
                                    
                                    <form onSubmit={(event) => {
                                        event.preventDefault();
                                    }} className="d-flex align-items-center justify-content-between col-12 col-xl-5">
                                        <ErrorMessage soft><strong>Remover livro do estoque</strong></ErrorMessage>
                                        <div className="p-0 w-50"><button className="pill-button-default w-100" data-toggle="modal" data-target="#removeBookModal">Remover Livro</button></div>
                                    </form>
                                </div>
                            </header>

                            <section className="d-flex col-12 p-0">
                                <aside className="d-flex flex-column align-items-center col-4">
                                    <div>
                                        {
                                            previewCoverUrl ? (
                                                <img src={previewCoverUrl} style={{objectFit: 'cover', pointerEvents: 'none'}} alt={"Capa do Livro: " + name} width="320" height="480" className="mb-1"/>
                                            ) : (
                                                <label htmlFor="image[]" style={{width: 320, height: 480, cursor: 'pointer'}} className="without-cover d-flex align-items-center justify-content-center mb-1">
                                                    <BsImage size={32} style={{color: "#fda085"}}/>
                                                    <BsPlus size={24} style={{color: "#fda085", position: "absolute", marginRight: -38, marginTop: -35}}/>
                                                </label>
                                            )
                                        }
                                    </div>

                                    <p className="text-small text-center"><small>Tamanho de imagem mínima recomendada: 320 x 480</small></p>
                                    <div className="d-flex w-100">
                                        {
                                            previewCoverUrl ? (
                                                <div className="d-flex w-100">
                                                    <button className="pill-button-default mx-2 my-3 w-50" onClick={removePreviewCoverUrl}>Cancelar envio</button>

                                                    <label htmlFor="image[]" className="pill-button mx-2 my-3 w-50 d-flex align-items-center justify-content-center">
                                                        Enviar Foto
                                                    </label>
                                                </div>
                                            ) : (
                                                <div className="d-flex justify-content-center w-100">
                                                    <label htmlFor="image[]" className="pill-button mx-2 my-3 w-50 d-flex align-items-center justify-content-center py-2">
                                                        Enviar Foto
                                                    </label>
                                                </div>
                                            )
                                        }
                                        
                                        <input className="d-none" type="file" id="image[]" accept="image/png, image/jpeg, image/bmp" onChange={event => {
                                            if (event.target.value) {
                                                handlePreviewCover(event)
                                            }
                                        }}/>
                                    </div>
                                </aside>

                                <div className="d-flex flex-column col-8">
                                    <h3>Informações do Livro</h3>

                                    <div className="d-flex p-0 mt-4">
                                        <div className="col-12 col-lg-4 p-0 pr-3"><Input label="Nome" placeholder="Nome do Livro" value={name} onChange={setName}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="ISBN" placeholder="ISBN" value={isbn} onChange={setIsbn} disabled/></div>
                                        <div className="col-12 col-lg-4 p-0 pl-3"><Input label="Páginas" type="number" placeholder="Quantidade de Páginas" value={pages} onChange={setPages}/></div>
                                    </div>

                                    <div className="d-flex justify-content-between p-0 mt-4 mb-4">
                                        <div className="col-12 col-lg-4 p-0 pr-3"><Input label="Autor" placeholder="Nome do Autor" value={author} onChange={setAuthor}/></div>
                                        <div className="col-12 col-lg-4 p-0 px-3"><Input label="Editora" placeholder="Nome da Editora" value={publishingCompany} onChange={setPublishingCompany}/></div>
                                        <div className="col-12 col-lg-4 p-0 pl-3"><Input label="Gênero" placeholder="Gênero" value={genre} onChange={setGenre}/></div>
                                    </div>

                                    <TextArea label="Sinopse" rows={5} value={synopsis} onChange={setSynopsis}/>

                                    <hr/>
                                    <h3>Informações do Estoque</h3>

                                    <div className="d-flex p-0 mt-4 mb-4">
                                        <div className="col-12 col-lg-6 p-0 pr-3"><Input label="Preço" type="number" placeholder="R$ 00,00" value={price} onChange={setPrice}/></div>
                                        <div className="col-12 col-lg-6 p-0 pl-3"><Input label="Quantidade em Estoque" placeholder="Unidades" value={quantity} onChange={setQuantity}/></div>
                                    </div>
                                </div>
                            </section>
                        </section>
                    </div>

                    <Footer>
                        <div className="d-flex align-items-center h-100">
                            <BsPencilSquare size={24} style={{marginLeft: 15, marginRight: 15, color: "#fda085"}}/>

                            <div className="d-flex flex-column">
                                <p>Para atualizar o livro, basta preencher os campos e clicar em "<u>Atualizar Livro</u>".</p>
                                <p>Você também pode remover um livro do estoque clicando no botão "<u>Remover Livro</u>".</p>
                            </div>
                        </div>
                        <div className="d-flex align-items-center h-100">
                            <form method="post" onSubmit={submitBookCreation}>
                                <button type="submit" id="new-book" className="pill-button">Atualizar Livro</button>
                            </form>
                        </div>
                    </Footer>
                </div>
            </div>
        </div>
    );

    function removeBookModal(book: Book, previewCoverUrl: string) {
        return (
            <section className="modal fade" id="removeBookModal" tabIndex={-1} role="dialog" aria-labelledby="removeBookModal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content pt-5 pb-3 px-5">
                        <div className="position-absolute close close-button" data-dismiss="modal" aria-label="Close">
                            <BsX />
                        </div>
    
                        <h4 className="text-center mb-4"><strong>Você tem certeza?</strong></h4>
    
                        <strong><p className="text-center mb-2">Você está removendo: <strong>{book.name}</strong> do estoque.</p></strong>
                        <strong><p className="text-center mb-3" style={{ color: "#FF9292" }}>Prejuízo estimado: <u>R$ {(book.quantity * book.price).toFixed(2)}</u>.</p></strong>
    
                        <div className="d-flex justify-content-center mb-3">
                            <img src={previewCoverUrl} alt={"Capa do Livro: " + book.name} style={{ width: 220, height: 330, pointerEvents: "none" }} />
                        </div>
    
                        <p className="text-center">Não se preocupe, remover um livro do estoque não<br/>irá apagar os registros de compras feitas pelos leitores.</p>
    
                        <form onSubmit={event => {
                            event.preventDefault();
                        }} className="d-flex align-items-center justify-content-center my-4">
                            <button className="pill-button-default mx-3" data-dismiss="modal" aria-label="Close">Não, mudei de ideia</button>
                            <button onClick={() => submitBookDestroy()} className="pill-button-danger mx-3" data-dismiss="modal" aria-label="Close">Sim, remover</button>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}
