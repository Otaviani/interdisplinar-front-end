import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { BsImage, BsPersonBoundingBox, BsPlus } from "react-icons/bs";
import { useHistory } from "react-router-dom";
import Input from "../components/Input";

import api from "../services/api";

import '../styles/components/pill-button.css';
import '../styles/pages/register.css';

interface Reader {
    name: string,
    birth: Date,
    phone: string,
    cpf: string,
    email: string,
    password: string,
    address: Address
}

interface Address {
    cep: string,
    city: string,
    state: string,
    street: string,
    neighborhood: string,
    number: string,
    complement?: string
}

export default function Register() {
    const history = useHistory();

    const [activeHeader, setActiveHeader] = useState(0);
    const [previewPhotoUrl, setPreviewPhoto] = useState<string>('');

    const [name, setName] = useState('');
    const [birth, setBirth] = useState('');
    const [phone, setPhone] = useState('');
    const [cpf, setCpf] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');

    const [cep, setCep] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [street, setStreet] = useState('');
    const [neighborhood, setNeighborhood] = useState('');
    const [number, setNumber] = useState('');
    const [complement, setComplement] = useState('');

    const [photo, setPhoto] = useState<File>(null as any);

    useEffect(() => {
        getAddressByCep(cep);
    }, [cep.length === 8]);

    function handlePreviewPhoto(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }

        setPhoto(event.target.files[0]);
        setPreviewPhoto(URL.createObjectURL(event.target.files[0]));
    }

    function removePreviewPhoto() {
        setPreviewPhoto('');
    }

    function getAddressByCep(cep: string) {
        api.get(`/register/via-cep/${cep}`).then(response => {
            setCep(response.data.cep);
            setCity(response.data.city);
            setNeighborhood(response.data.neighborhood);
            setState(response.data.state_address);
            setStreet(response.data.address);
        });
    }

    async function registerReader(event: FormEvent) {
        event.preventDefault();

        const data = new FormData();

        data.append('name', name);
        data.append('birth', birth);
        data.append('telephone', phone);
        data.append('cpf', cpf);
        data.append('email', email);
        data.append('password', password);

        data.append('cep', cep);
        data.append('city', city);
        data.append('state', state);
        data.append('street', street);
        data.append('neighborhood', neighborhood);
        data.append('number', number);
        data.append('complement', complement);

        data.append('photoFile', photo);

        await api.post('/reader/create', data, {
            headers: {
                'Content-Type': 'multipart/formdata',
            }
        });
        
        history.push('/login');
    }

    return (
        <div id="login">
            <section className="col-12 col-sm-10 col-md-8 col-lg-5 col-offset-1 p-5">
                <div id="carouselExampleIndicators" className="carousel slide w-100" data-ride="carousel" data-interval={false} style={{ height: 475 }}>
                    <ol className="d-flex w-100 align-items-center justify-content-between m-0">
                        <li className={ (activeHeader === 0 ? "register-border-active" : "register-border") + " w-100 text-center py-2 active"} data-target="#carouselExampleIndicators" data-slide-to="0" onClick={() => setActiveHeader(0)}>Informações</li>
                        <li className={ (activeHeader === 1 ? "register-border-active" : "register-border") + " w-100 text-center py-2"} data-target="#carouselExampleIndicators" data-slide-to="1" onClick={() => setActiveHeader(1)}>Endereço</li>
                        <li className={ (activeHeader === 2 ? "register-border-active" : "register-border") + " w-100 text-center py-2"} data-target="#carouselExampleIndicators" data-slide-to="2" onClick={() => setActiveHeader(2)}>Foto</li>
                    </ol>

                    <div className="carousel-inner h-100">
                        <div className="carousel-item active h-100 px-3">
                            <form>
                                <h5 className="mt-4 mb-2"><strong>Informações Pessoais:</strong></h5>
                                <div className="d-flex mt-2">
                                    <div className="w-100 mr-3"><Input label="Nome completo" type="text" placeholder="Nome" value={name} onChange={setName}/></div>
                                    <div className="w-100 ml-3"><Input label="Data de nascimento" type="text" placeholder="Data de nascimento" value={birth} onChange={setBirth}/></div>
                                </div>

                                <div className="d-flex mt-2">
                                    <div className="w-100 mr-3"><Input label="Telefone" type="text" placeholder="Telefone" value={phone} onChange={setPhone}/></div>
                                    <div className="w-100 ml-3"><Input label="CPF" type="text" placeholder="CPF" value={cpf} onChange={setCpf}/></div>
                                </div>

                                <h5 className="mt-4 mb-2"><strong>Informações de Acesso:</strong></h5>
                                <div className="d-flex mt-2">
                                    <Input label="Email" type="text" placeholder="Email" value={email} onChange={setEmail}/>
                                </div>

                                <div className="d-flex mt-2">
                                    <div className="w-100 mr-3"><Input label="Senha" type="password" placeholder="Senha" value={password} onChange={setPassword}/></div>
                                    <div className="w-100 ml-3"><Input label="Confirmar Senha" type="password" placeholder="Confirmar senha" value={passwordConfirmation} onChange={setPasswordConfirmation}/></div>
                                </div>

                                <div className="d-flex align-items-center justify-content-center mt-4">
                                    <button className="pill-button w-50" data-target="#carouselExampleIndicators" data-slide-to="1" onClick={() => setActiveHeader(1)}>Próximo passo</button>
                                </div>
                            </form>
                        </div>

                        <div className="carousel-item h-100 px-3">
                            <form>
                                <h5 className="mt-4 mb-2"><strong>Região:</strong></h5>
                                <div className="d-flex mt-2">
                                    <div className="w-25 mr-3"><Input label="CEP" type="text" placeholder="CEP" value={cep} onChange={setCep}/></div>
                                    <div className="w-50 mx-3"><Input label="Cidade" type="text" placeholder="Cidade" value={city} onChange={setCity}/></div>
                                    <div className="w-25 ml-3"><Input label="Estado" type="text" placeholder="Estado" value={state} onChange={setState}/></div>
                                </div>

                                <h5 className="mt-4 mb-2"><strong>Endereço:</strong></h5>
                                <div className="d-flex mt-2">
                                    <Input label="Logradouro" type="text" placeholder="Logradouro" value={street} onChange={setStreet}/>
                                </div>

                                <div className="d-flex mt-2">
                                    <div className="w-100 mr-3"><Input label="Bairro" type="text" placeholder="Bairro" value={neighborhood} onChange={setNeighborhood}/></div>
                                    <div className="w-100 ml-3"><Input label="Número" type="number" placeholder="Número" value={number} onChange={setNumber}/></div>
                                </div>

                                <div className="d-flex mt-2">
                                    <Input label="Complemento" type="text" placeholder="Complemento" value={complement} onChange={setComplement}/>
                                </div>

                                <div className="d-flex align-items-center justify-content-center mt-4">
                                    <button className="pill-button-default mx-2 w-25" data-target="#carouselExampleIndicators" data-slide-to="0" onClick={() => setActiveHeader(0)}>Voltar</button>
                                    <button className="pill-button mx-2 w-25" data-target="#carouselExampleIndicators" data-slide-to="2" onClick={() => setActiveHeader(2)}>Próximo passo</button>
                                </div>
                            </form>
                        </div>

                        <div className="carousel-item h-100 px-3">
                            <form className="d-flex flex-column justify-content-center h-100" onSubmit={event => registerReader(event)}>
                                <p className="text-center mb-4">Clique no ícone abaixo para selecionar uma foto para o seu perfil!</p>
                                <div className="d-flex justify-content-center">
                                    {
                                        previewPhotoUrl ? (
                                            <label htmlFor="image" className="position-relative profile-photo">
                                                <img src={previewPhotoUrl} style={{objectFit: 'cover', pointerEvents: 'none'}} alt="Foto do funcionário" className="photo my-2"/>
                                            </label>
                                        ) : (
                                            <label htmlFor="image" style={{cursor: 'pointer'}} className="photo without-photo d-flex align-items-center justify-content-center my-2">
                                                <BsPersonBoundingBox size={32} style={{color: "#fda085"}}/>
                                            </label>
                                        )
                                    }
                                </div>
                                  
                                <input className="d-none" type="file" id="image" accept="image/png, image/jpeg, image/bmp" onChange={event => {
                                    if (event.target.value) {
                                        handlePreviewPhoto(event)
                                    }
                                }}/>

                                <div className="d-flex align-items-center justify-content-center mt-4">
                                    <button className="pill-button-default mx-2 w-25" data-target="#carouselExampleIndicators" data-slide-to="1" onClick={() => setActiveHeader(1)}>Voltar</button>
                                    <button className="pill-button mx-2 w-25" onClick={() => setActiveHeader(2)}>Finalizar cadastro</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}