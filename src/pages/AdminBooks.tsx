import React, { useEffect, useState } from "react";

import '../styles/pages/admin-books.css';

import api from "../services/api";
import BookAdminCard from "../components/card/BookAdminCard";
import Menu from "../components/Menu";
import Filter from "../components/Filter";

interface Book {
    "uuid": string,
    "isbn": string,
    "name": string,
    "synopsis": string,
    "genre": string,
    "cover": string,
    "quantity": number,
    "price": number,
}

export default function ItemsCart() {
    const [books, setBooks] = useState<Book[]>([]);
  
    useEffect(() => {
        api.get('https://run.mocky.io/v3/f6f039f2-600c-416d-b7b6-b09411848ae4').then(response => {
            setBooks(response.data.books);
        });
    }, []);

    if (!books) {
        return <p>Carregando...</p>
    }

    return (
        <div id="admin-books">
            <Menu active="settings"/>

            <main>
                <section>
                    <Filter />

                    <div className="list-books">
                        {books.map(book => {
                            return (
                                <BookAdminCard key={book.uuid} {...book}/>
                            );
                        })}
                    </div>
                </section>
            </main>
        </div>
    );
}