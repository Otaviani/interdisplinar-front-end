import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/Login';
import ListBooks from './pages/ListBooks';
import ItemsCart from './pages/ItemsCart';
import ShowBook from './pages/ShowBook';
import ConfigListBooks from './pages/config/ConfigListBooks';
import ConfigEmployees from './pages/config/ConfigEmployees';
import Reports from './pages/config/Reports';
import CreateBook from './pages/config/CreateBook';
import UpdateBook from './pages/config/UpdateBook';
import Admission from './pages/config/Admission';
import UpdateEmployee from './pages/config/UpdateEmployee';
import Register from './pages/Register';
import Profile from './pages/Profile';

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/login" exact component={Login} />
                <Route path="/cadastro" exact component={Register} />
                <Route path="/livros" exact component={ListBooks} />
                <Route path="/livros/:uuid" exact component={ShowBook} />
                <Route path="/carrinho" exact component={ItemsCart} />
                <Route path="/perfil" exact component={Profile} />
                <Route exact path="/configuracoes">{<Redirect to="/configuracoes/livros" />}</Route>
                <Route path="/configuracoes/livros" exact component={ConfigListBooks} />
                <Route path="/configuracoes/livros/cadastrar-livro" exact component={CreateBook} />
                <Route path="/configuracoes/livros/:uuid" exact component={UpdateBook} />
                <Route path="/configuracoes/funcionarios" exact component={ConfigEmployees} />
                <Route path="/configuracoes/funcionarios/cadastrar-funcionario" exact component={Admission} />
                <Route path="/configuracoes/funcionarios/:uuid" exact component={UpdateEmployee} />
                <Route path="/configuracoes/relatorios" exact component={Reports} />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
