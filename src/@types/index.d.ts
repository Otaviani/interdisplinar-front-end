declare module "*.png";
declare module "*.svg";
declare module "*.jpg";

interface LoadingProps {
    isLoading: boolean;
}

interface JQuery {
    daterangepicker(options?: any, callback?: Function) : any;
 }