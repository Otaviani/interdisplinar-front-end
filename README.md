## Pré-Requisitos:

[Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)  
[Chocolatey](https://chocolatey.org/install)  
[Node.js](https://nodejs.org/en/download/package-manager/#windows) (Download via Chocolatey)

---
### Clone o repositório:
```
git clone https://gitlab.com/Otaviani/interdisplinar-front-end.git
```

### Execução:
---
``` 
yarn install
```
``` 
yarn start
```

### Acesse:
---
[http://localhost:3000/login](http://localhost:3000/login)